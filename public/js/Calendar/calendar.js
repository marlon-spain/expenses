    $(document).ready(function() {
        var selectedDates = [];

        $('#date').datepicker({
            format: 'yyyy-mm-dd', // Formato de la fecha
            autoclose: false, // Mantener abierto después de seleccionar la fecha
            clearBtn: true, // Mostrar botón para limpiar la selección
            multidate: 2, // Permitir seleccionar solo dos fechas
            multidateSeparator: ' - ', // Separador para el rango de fechas
            beforeShow: function(input, inst) {
                if (selectedDates.length === 2) {
                    selectedDates = []; // Limpiar las fechas seleccionadas
                    inst.clearDates(); // Borrar las fechas del calendario
                }
            }
        }).on('changeDate', function(e) {
            selectedDates = e.dates;

            if (selectedDates.length === 2) {
                $(this).datepicker('hide');
            }
        });
    });
