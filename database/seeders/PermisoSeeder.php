<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermisoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Reset cached roles and permissions
         app()[PermissionRegistrar::class]->forgetCachedPermissions();

         // create permissions
         Permission::create(['name' => 'ver::user']);
         Permission::create(['name' => 'crear::user']);
         Permission::create(['name' => 'editar::user']);
         Permission::create(['name' => 'borrar::user']); 
         
         Permission::create(['name' => 'ver::rol']);
         Permission::create(['name' => 'crear::rol']);
         Permission::create(['name' => 'editar::rol']);
         Permission::create(['name' => 'borrar::rol']);

         Permission::create(['name' => 'ver::permiso']);
         Permission::create(['name' => 'crear::permiso']);
         Permission::create(['name' => 'editar::permiso']);
         Permission::create(['name' => 'borrar::permiso']);

         Permission::create(['name' => 'ver::log']);
         Permission::create(['name' => 'ver::kpis']);
         Permission::create(['name' => 'ver::api']);
         Permission::create(['name' => 'ver::session']);
        
 
         // create roles and assign existing permissions
         $role1 = Role::create(['name' => 'Super-Admin']);

         $role2 = Role::create(['name' => 'Admin']);
         $role2->givePermissionTo('ver::user');
         $role2->givePermissionTo('crear::user');
         $role2->givePermissionTo('editar::user');
         $role2->givePermissionTo('borrar::user');
         $role2->givePermissionTo('ver::rol');
         $role2->givePermissionTo('crear::rol');
         $role2->givePermissionTo('editar::rol');
         $role2->givePermissionTo('borrar::rol');
         $role2->givePermissionTo('ver::permiso');
         $role2->givePermissionTo('crear::permiso');
         $role2->givePermissionTo('editar::permiso');
         $role2->givePermissionTo('borrar::permiso');
         $role2->givePermissionTo('ver::log');
         $role2->givePermissionTo('ver::kpis');
         $role2->givePermissionTo('ver::api');
         $role2->givePermissionTo('ver::session');        

         $role3 = Role::create(['name' => 'Privilegios']); 
         $role3->givePermissionTo('ver::rol');
         $role3->givePermissionTo('crear::rol');
         $role3->givePermissionTo('editar::rol');
         $role3->givePermissionTo('borrar::rol');
         $role3->givePermissionTo('ver::permiso');
         $role3->givePermissionTo('crear::permiso');
         $role3->givePermissionTo('editar::permiso');
         $role3->givePermissionTo('borrar::permiso');
         
         $role4 = Role::create(['name' => 'User']);         
         $role4->givePermissionTo('ver::api');
         $role4->givePermissionTo('ver::session'); 
 
         
         // gets all permissions via Gate::before rule; see AuthServiceProvider

         $user = User::factory()->create([
            'name' => 'Super-Admin User',
            'email' => 'superadmin@backoffice.es',
            'password' => bcrypt('031132'),
           // 'profile_photo_path' => 'profile-photos/6LZ1TLXFaB9R0hS4I6fm10IB1tc5kmasMcZZk5xZ.png'
        ]);
        $user->assignRole($role1);
 
         // create demo type users
         $user = User::factory()->create([
             'name' => 'Admin user',
             'email' => 'admin@admin.es',
             'password' => bcrypt('12345678'),
           //  'profile_photo_path' => 'profile-photos/6LZ1TLXFaB9R0hS4I6fm10IB1tc5kmasMcZZk5xZ.png'             
         ]);
         $user->assignRole($role2);
 
         $user = User::factory()->create([
             'name' => 'Gestion de roles y permisos',
             'email' => 'moder@moder.es',
             'password' => Hash::make('031132'),
           //  'profile_photo_path' => 'profile-photos/eY943CG1r0kEpDZwk74nBW2790PJWjkHdcLcmJzb.png' 
         ]);
         $user->assignRole($role3);

         //users
         $user = User::factory()->create([
            'name' => 'usuario',
            'email' => 'user@user.es',
            'password' => Hash::make('031132'),             
        ]);
        $user->assignRole($role4);
 
        $user = User::factory()->create([
            'name' => 'Marlon Briones',
            'email' => 'marlon_b91@hotmail.com',
            'password' => Hash::make('031132'),               
        ]);
        $user->assignRole($role4);

        $user = User::factory()->create([
            'name' => 'Rafael Castro',
            'email' => 'zurdo91.mbc@gmail.com',
            'password' => Hash::make('031132'),   
            
        ]);
        $user->assignRole($role4);

        $user = User::factory()->create([
            'name' => 'Marlon Synlab',
            'email' => 'marlon.briones@synlab.es',
            'password' => Hash::make('031132'),                      
        ]);
        $user->assignRole($role4);
    }
}
