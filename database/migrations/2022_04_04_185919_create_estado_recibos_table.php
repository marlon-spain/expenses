<?php

use App\Models\Estado_recibo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado_recibos', function (Blueprint $table) {             
              $table->id();
              $table->string('nombre', 45)->nullable();
              $table->timestamps();
        });

        $toCreate = [
            [
                'nombre' => 'Pagada',                
            ],
            [
                'nombre' => 'Devuelto',                
            ],
            [
                'nombre' => 'Impagado',                
            ],
            [
                'nombre' => 'Aplazado',                
            ],
            [
                'nombre' => 'Fraccionado',               
            ],
            [
                'nombre' => 'Cancelado',                
            ],

        ];
        foreach ($toCreate as $create) Estado_recibo::create($create);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estado_recibos');
    }
};
