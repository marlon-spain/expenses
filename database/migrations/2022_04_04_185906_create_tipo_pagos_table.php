<?php

use App\Models\Tipo_pago;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_pagos', function (Blueprint $table) {
            //mensual anual semestral 
            $table->id();
            $table->string('nombre', 45)->nullable();
            $table->timestamps();
        });

        $toCreate = [
            [
                'nombre' => 'Tarjeta de debito',
    
            ],
            [
                'nombre' => 'Tarjeta de credito',
           
            ],
            [
                'nombre' => 'Bizum',
             
            ],
            [
                'nombre' => 'Movil o NFC',
            
            ],
            [
                'nombre' => 'PayPal',
            
            ],
            [
                'nombre' => 'Cheque',            
            ],
            [
                'nombre' => 'Transferencia Bancaria',
            
            ],
        ];
        foreach ($toCreate as $create) Tipo_pago::create($create);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_pagos');
    }
};
