<?php

use App\Models\Empresa;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',150)->nullable();
            $table->timestamps();
        });
        $toCreate = [
          
            [
                'nombre' => 'SYNLAB',                
            ],          
            [
                'nombre' => 'Trinitech',                
            ],
            [
                'nombre' => 'Seidor',                
            ],
            [
                'nombre' => 'AMB',                
            ],
            [
                'nombre' => 'TKM',                
            ],
            [
                'nombre' => 'Retractil91',                
            ],
            [
                'nombre' => 'Akoma',                
            ],

        ];
        foreach ($toCreate as $create) Empresa::create($create);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
};
