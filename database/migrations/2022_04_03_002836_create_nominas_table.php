<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('nominas')) {
        Schema::create('nominas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->date('fecha')->nullable();
            $table->float('devengo', 8, 2)->nullable();
            $table->float('deducciones', 8, 2)->nullable();
            $table->integer('IRPF')->nullable();
            $table->float('remuneracion', 8, 2)->nullable();
            $table->float('restante', 8, 2)->nullable();
            $table->string('comentarios', 150)->nullable();
            $table->unsignedBigInteger('empresa_id')->nullable();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
        });
    }
    if (Schema::hasTable('nominas')) {
        if (!Schema::hasColumn('nominas', 'restante'))
        {           
            Schema::table('nominas', function (Blueprint $table) {
                $table->float('restante')->nullable()->after('remuneracion');
                });      
           
        } 
    }

}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nominas');
    }
};
