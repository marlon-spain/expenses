<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recibos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->date('fecha')->nullable();
            $table->string('descripcion', 100)->nullable();
            $table->float('importe', 8, 2)->nullable();
            $table->unsignedBigInteger('cuenta_id')->nullable();
            $table->foreign('cuenta_id')->references('id')->on('cuentas');
            $table->unsignedBigInteger('tipo_gasto_id')->nullable();
            $table->foreign('tipo_gasto_id')->references('id')->on('tipo_gastos');
            $table->unsignedBigInteger('gasto_fijo_id')->nullable();
            $table->foreign('gasto_fijo_id')->references('id')->on('gasto_fijos');
            $table->unsignedBigInteger('estado_id')->nullable();
            $table->foreign('estado_id')->references('id')->on('estado_recibos');
            $table->unsignedBigInteger('tipo_pago_id')->nullable();
            $table->foreign('tipo_pago_id')->references('id')->on('tipo_pagos');
            $table->unsignedBigInteger('nomina_id')->nullable();
            $table->foreign('nomina_id')->references('id')->on('nominas');
            $table->unsignedBigInteger('deuda_id')->nullable();
            $table->foreign('deuda_id')->references('id')->on('deudas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recibos');
    }
};
