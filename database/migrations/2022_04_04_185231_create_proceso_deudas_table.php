<?php

use App\Models\Proceso_deuda;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proceso_deudas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',45)->nullable();
            $table->timestamps();
        });

        $toCreate = [
          
            [
                'nombre' => 'Mensual',
                
            ],
            [
                'nombre' => 'Anual',
                
            ],
            [
                'nombre' => 'Semestral',
                
            ],
            [
                'nombre' => 'Trimestral',
               
            ],          

        ];
        foreach ($toCreate as $create) Proceso_deuda::create($create);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proceso_deudas');
    }
};
