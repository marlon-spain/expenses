<?php

use App\Models\Gasto_fijo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasto_fijos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',45)->nullable();
            $table->timestamps();
        });
        $toCreate = [
            [
                'nombre' => 'Hipoteca',                
            ],
            [
                'nombre' => 'Agua',                
            ],
            [
                'nombre' => 'Luz',                
            ],
            [
                'nombre' => 'internet, telefono',                
            ],
            [
                'nombre' => 'Movil',               
            ],
            [
                'nombre' => 'ocio',                
            ],
            [
                'nombre' => 'Gasolina',                
            ],

            [
                'nombre' => 'Deporte',                
            ],

            [
                'nombre' => 'Streaming, pc, tv',                
            ],
            [
                'nombre' => 'Otros',                
            ],


        ];
        foreach ($toCreate as $create) Gasto_fijo::create($create);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasto_fijos');
    }
};
