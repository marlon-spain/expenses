<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('deudas')) {
        Schema::create('deudas', function (Blueprint $table) {
            $table->id();  
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->date('fecha_inicio')->nullable();
            $table->string('nombre', 45)->nullable();
            $table->date('fecha_fin')->nullable();
            $table->float('total', 8, 2)->nullable();
            $table->float('restante', 8, 2)->nullable();
            $table->unsignedBigInteger('estado_deuda_id')->nullable();
            $table->foreign('estado_deuda_id')->references('id')->on('estado_deudas');
            $table->unsignedBigInteger('Proceso_deuda_id')->nullable();
            $table->foreign('proceso_deuda_id')->references('id')->on('proceso_deudas');
            $table->timestamps();
        });
    }

    if (Schema::hasTable('deudas')) {
        if (!Schema::hasColumn('deudas', 'total'))
        {           
            Schema::table('deudas', function (Blueprint $table) {
                $table->float('total')->nullable()->after('fecha_fin');
                });      
           
        } 
    }

    if (Schema::hasTable('deudas')) {
        if (!Schema::hasColumn('deudas', 'restante'))
        {           
            Schema::table('deudas', function (Blueprint $table) {
                $table->float('restante')->nullable()->after('total');
                });      
           
        } 
    }


}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deudas');
    }
};
