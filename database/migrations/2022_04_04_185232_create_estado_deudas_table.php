<?php

use App\Models\Estado_deuda;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //pagando parado por pagar 
        Schema::create('estado_deudas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 45)->nullable();
            $table->timestamps();
        });

        $toCreate = [
          
            [
                'nombre' => 'En Proceso',
                
            ],
            [
                'nombre' => 'Finalizada',
                
            ],
            [
                'nombre' => 'Cancelada',
                
            ],

        ];
        foreach ($toCreate as $create) Estado_deuda::create($create);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estado_deudas');
    }
};
