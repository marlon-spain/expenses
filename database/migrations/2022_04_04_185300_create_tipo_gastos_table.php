<?php

use App\Models\Tipo_gasto;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_gastos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 45)->nullable();
            $table->string('descripcion', 300)->nullable();
            $table->timestamps();
        });

        $toCreate = [
            [
                'nombre'=>'Gastos fijos',
                'descripcion'=>'Se entienden por gastos fijos aquellas cantidades que debemos pagar de
                 forma periódica y cuyo importe conocemos de antemano.',
            ],
            [
            'nombre'=>'Gastos variables',
            'descripcion'=>'Dentro de los gastos variables se engloban todos aquellos que dependen del consumo para fijar el importe final. 
            Aquí podríamos englobar la luz, el agua o el teléfono, así como la gasolina de nuestro coche',
            ],
            [
                'nombre'=>'Gastos emergencia',
                'descripcion'=>'El fondo de emergencia es tu colchón ante imprevistos.
                 Es un dinero que servirá para cubrir los gastos no presupuestados que puedan surgir en el día a día. ',
                ],
                [
                    'nombre'=>'Gastos flexibles',
                    'descripcion'=>'¿Cuáles son los gastos flexibles?
                    Una cuenta de gastos flexibles es una cuenta con ventajas impositivas que le permite usar su dinero antes de
                     impuestos para pagar gastos calificados de atención médica o de cuidado de dependientes.',
                    ],
                    [
                        'nombre'=>'Gastos discrecionales',
                        'descripcion'=>'Gastos discrecionales son aquellos para los que deliberadamente hemos asignado 
                        una cifra determinada. Por ejemplo, lo que dedicamos mensualmente a ropa, 
                        calzado, productos de belleza, salidas y entretenimiento, entre otras categorías.',
                        ],        
                        [
                            'nombre'=>'Gastos hormiga',
                            'descripcion'=>'Se conoce como «gastos hormiga» a aquellos gastos que no suponen un desembolso muy elevado,
                             pero que, sumados día tras día, acaban dando lugar a un importe más que considerable.',
                            ],                 
        ];
        foreach ($toCreate as $create) Tipo_gasto::create($create);
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_gastos');
    }
};
