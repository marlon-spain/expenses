<?php

use App\Events\SendMessage;
use App\Events\GraficoEvent;
use App\Events\PublicMessage;
use App\Events\PrivateMessage;
use App\Events\CasillacheckEvent;
use App\Http\Controllers\ActivityLogController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KpisController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DeudaController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\CuentaController;
use App\Http\Controllers\NominaController;
use App\Http\Controllers\ReciboController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\PermisoController;
use App\Http\Controllers\SeguimientoController;
use App\Http\Controllers\TimeUserSessionController;



Route::middleware('RegisterLog')->group(function () { //Registro de actividad Logs
    Route::resource('log', ActivityLogController::class);

    Route::get('/', function () {
        return view('auth.login');
        //return view('welcome');
    });

    Route::get('/chat', function () {
        event(new PublicMessage());
        return view('socket');
    });

    Route::get('/disparo', function () {
        return view('webSocket.socket');
        //return view('welcome');
    });

    Route::get('/socket', function () {
        $event = event(new SendMessage("hola"));
        $event = event(new PublicMessage("publico"));
        //$user = auth()->user();
        //$event = event(new PrivateMessage($user));
        return ('Event Run Successfully.');
    });

    //se debe pasar los datos al websocket por esta ruta
    Route::get('/grafico', function () {
        $event = event(new GraficoEvent("hhhh", null, null));
        return ('Event Run Successfully.');
    });


    Route::group(['middleware' => ['auth']], function () {
        //reidrect user segun rol
        Route::get('/', [HomeController::class, 'index'])->name('home');
        Route::resource('nomina', NominaController::class);
        
        Route::resource('deuda', DeudaController::class);
        Route::resource('pago', CuentaController::class);
        Route::resource('cuenta', CuentaController::class);
        Route::put('cuenta/change/{id}', [CuentaController::class, 'changeStatus']);
        Route::resource('recibo', ReciboController::class);
        Route::resource('user', UserController::class);
        Route::post('user/action', [UserController::class, 'actionUser']);
        

        Route::resource('empresa', EmpresaController::class);
        Route::resource('timeUserSession', TimeUserSessionController::class);
        Route::resource('seguimiento', SeguimientoController::class);
        Route::resource('roles', RoleController::class);
        Route::resource('permisos', PermisoController::class);

        Route::controller(KpisController::class)->group(function () {
            Route::get('kpis', 'getTotalrecibo');
            Route::get('kpis/action', 'action')->name('kpis.action');
            Route::get('kpis/inicio', 'inicio')->name('kpis.inicio');
            Route::get('kpis/search', 'search')->name('kpis.search');
            Route::post('kpis/filter', 'searchFilter')->name('kpis.searchFilter');
        });

        //full calendar
        Route::get('calendar', [EventController::class, 'index'])->name('calendar.index');
        Route::post('calendar/create-event', [EventController::class, 'create'])->name('calendar.create');
        Route::post('calendar/edit-event', [EventController::class, 'edit'])->name('calendar.edit');
        Route::delete('calendar/remove-event', [EventController::class, 'destroy'])->name('calendar.destroy');
    });
}); //cierre log

//route:clear -- optimize:clear  //route-list
