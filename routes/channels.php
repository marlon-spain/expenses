<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/


Broadcast::channel( 'App.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('message-channel.{id}', function ($user, $id) {
   //el id del ususario es igual al id autentificado
    return (int) $user->id === (int) $id;
});

//porque necesito un canal para cada evento privado
Broadcast::channel('casilla-channel.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});