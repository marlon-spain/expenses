## install aplicationbackoffice
- git clone git@bitbucket.org:marlon-spain/backoffice.git
- crear .env
- composer install
- php artisan config:cache
- php artisan key:generate
- php artisan storage:link
- php artisan migrate
- php artisan serve
-config permisos carpeta vendor/node_modules
- npm install
- npm run dev
## activarl a sessiones de navegador
SESSION_DRIVER=database
- php artisan config:cache
- php artisan optimize

## config adminlt3
https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
si no coge la configuracion 
- php artisan adminlte:install --only=auth_views
- php artisan route:view

## para usar jetstream y livewire
https://jetstream.laravel.com/2.x/building-your-app.html#team-settings-screen
- php artisan vendor:publish --tag=jetstream-views

## api sanctum
https://www.youtube.com/watch?v=WLY196kq07U

## multiuser redireccion segun rol
https://stackoverflow.com/questions/70157894/redirect-laravel-8-fortify

## socket io client laravel echo
https://www.itsolutionstuff.com/post/laravel-broadcast-redis-socket-io-tutorial-example.html
https://github.com/savanihd/Laravel-Broadcast-Redis-Socket-io-Tutorial-/blob/master/resources/views/welcome.blade.php
laravel-echo-server start
npm run watch
php artisan config:clear -- cuando no pilla el puerto en el .env el websocket


## ignorar fichero git
git rm -r --cached . 
crear un supervisor web socket
https://stackoverflow.com/questions/54811044/why-i-get-this-error-laravel-worker-error-no-such-group
configurar laravel echo server 
https://stackoverflow.com/questions/63827158/laravel-echo-server-and-neterr-ssl-protocol-error-on-https-version

supervisor levantar 

supervisorctl update
supervisorctl start b2b-echo

## combinacion de colas y difuciones del evento
Implementar --ShouldBroadcastNow:-- Esto significa que el evento se difundirá inmediatamente sin utilizar colas. Es útil cuando deseas que la difusión del evento ocurra de manera rápida y no necesitas procesamiento adicional en segundo plano.

Implementar --ShouldQueue:-- Esto significa que el evento se colocará en una cola para su procesamiento posterior. Es útil cuando deseas manejar el evento en segundo plano y no requieres una difusión inmediata. Puedes definir la conexión y la cola específica utilizando los métodos onConnection y onQueue en el evento.

Implementar tanto --ShouldBroadcastNow como ShouldQueue:-- Esto permite combinar la difusión inmediata y el procesamiento en segundo plano. El evento se difundirá inmediatamente y también se colocará en una cola para su procesamiento posterior. Puedes usar esto cuando deseas tener una respuesta rápida al cliente y también realizar tareas adicionales en segundo plano.