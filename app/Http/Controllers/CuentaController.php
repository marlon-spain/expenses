<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cuenta as model;
use App\Events\CasillacheckEvent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CuentaController extends Controller
{

    public function index(Request $request)
    {      

        if ($request->ajax()) {
            $items = model::select('*')->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();

            return datatables()->of($items)->addColumn('active', function ($item) {
                return '
                <form method="POST" style="display:inline" action="/cuenta/change/'.$item->id.'" id="delete-form-'.$item->id.'">
                    '.csrf_field().'
                    '.method_field('PUT').'
                    <input type="hidden" name="active" value="'.($item->active ? 1 : 0).'">
                    <input type="checkbox" value="1" id="item_active_checkbox_'.$item->id.'" '.($item->active ? 'checked' : '').' onchange="updateActiveStatus(this, '.$item->id.')">
                </form>';
            })
            ->addColumn('action', function ($item) {
                return '
                <a href="'.route('cuenta.edit', $item->id).'" class="btn btn-info btn-sm"> Editar </a>
                <button type="button" name="delete" data-id="'.$item->id.'" class="delete btn btn-danger btn-sm"> Eliminar </button>';
            })
            ->rawColumns(['active', 'action'])
            ->toJson();
        }
        return view('cuenta.index');
    }


    public function create()
    {
        $crear = model::get();
        return view('cuenta.create', compact('crear'));
    }


    public function store(Request $request)
    {
        $this->validate($request, ['nombre' => 'required', 'IBAN' => 'required']);
        $user = Auth::user()->id;
        $store = model::create([
            'user_id' => $user,
            'nombre' => $request->input('nombre'),
            'IBAN' => $request->input('IBAN'),
            'tipo' => $request->input('tipo'),
            'active' => $request->input('active')
        ]);
        $store->save();

        return redirect()->route('cuenta.index');
    }


    public function show($id)
    {
        $item = model::find($id);
    }

    public function edit($id)
    {
        $edit = model::find($id);

        return view('cuenta.edit', compact('edit'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, ['nombre' => 'required', 'IBAN' => 'required']);

        $update = model::find($id);
        $update->nombre = $request->input('nombre');
        $update->IBAN = $request->input('IBAN');
        $update->tipo = $request->input('tipo');
        $update->active = $request->input('active');
        $update->save();

        return redirect()->route('cuenta.index');
    }

    public function changeStatus(Request $request, $id)
    {
        // return $request->input('active');
        $cuenta = model::where('id', $id)->update([
            "active" => $request->input('active')
        ]);
        return redirect()->back();
    }

    public function destroy($id)
    {
        $item = model::findOrFail($id);
        $item->delete();
        return response()->json(['success' => true]);
    }
}
