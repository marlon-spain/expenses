<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-rol|crear-rol|editar-rol|borrar-rol', ['only' => ['index']]);
        $this->middleware('permission:crear-rol', ['only' => ['create', 'store']]);
        $this->middleware('permission:editar-rol', ['only' => ['edit', 'update']]);
        $this->middleware('permission:borrar-rol', ['only' => ['destroy']]);
    }
    /*
     @can('editar::rol')
    @endcan

    @can('borrar::rol')

    @endcan
    */

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = Role::all();
            return datatables()->of($items)->addColumn('action', function ($item) {
                return '<a href="' . route('roles.edit', $item->id) . '" class="btn btn-info btn-sm"> Editar 
                           </a>&nbsp;&nbsp;<button type="button" name="delete" data-id="' . $item->id . '" class="delete btn btn-danger btn-sm"> Eliminar </button>';
            })
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('roles.index');
    }


    public function create()
    {
        $permission = Permission::get();
        return view('roles.create', compact('permission'));
    }


    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'permission' => 'required']);
        $role = RoLe::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));

        return redirect()->route('roles.index');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $role = RoLe::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table('role_has_permissions')->where('role_has_permissions.role_id', $id)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();
        return view('roles.edit', compact('role', 'permission', 'rolePermissions'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'permission' => 'required']);

        $role = RoLe::find($id);
        $role->name = $request->input('name');
        $role->save();

        $role->syncPermissions($request->input('permission'));
        return redirect()->route('roles.index');
    }


    public function destroy($id)
    {
        $item = RoLe::findOrFail($id);
        $item->delete();
        return response()->json(['success' => true]);
    }
}
