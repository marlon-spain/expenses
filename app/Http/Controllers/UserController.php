<?php

namespace App\Http\Controllers;

use App\Models\ActivityLog;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\User as model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-user|crear-rol|editar-rol|borrar-rol', ['only' => ['index']]); //solo tenra permiso para ejecutar el metodo index
        $this->middleware('permission:crear-user', ['only' => ['create', 'store']]); //solo tenra permiso para ejecutar el metodo crear
        $this->middleware('permission:editar-user', ['only' => ['edit', 'update']]); //solo tenra permiso para ejecutar el metodo editar
        $this->middleware('permission:borrar-user', ['only' => ['destroy']]); //solo tenra permiso para ejecutar el metodo destroy
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = model::with('roles')->get();
            foreach ($items as $user) {
                $user['rol_name'] = $user->roles[0]->name ?? '';
                unset($user->roles);
                // Haz lo que necesites con el nombre del rol del usuario, por ejemplo:
                // ...
            }
            return datatables()->of($items)->addColumn('action', function ($item) {
                return '<a href="' . route('user.edit', $item->id) . '" class="btn btn-info btn-sm"> Editar 
                           </a>&nbsp;&nbsp;<button type="button" name="delete" data-id="' . $item->id . '" class="delete btn btn-danger btn-sm"> Eliminar </button>';
            })
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('user.index');
    }

    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        return view('user.create', compact('roles'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "email" => "required|email|unique:users,email",
            "password" => "required|same:confirm-password",
            "roles" => "required"
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = model::create($input);
        $user->assignRole($request->input('roles')); //asignar roles a usuarios

        return redirect()->route('user.index');
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $user = model::find($id);
        $roles = Role::pluck("name", "name")->all();
        $userRole = $user->roles->pluck("name", "name")->all();
        $permissions = $user->getAllPermissions()->pluck('name', 'name')->all();

        

        return view('user.edit', compact('user', 'roles', 'userRole','permissions'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required",
            "email" => "required|email|unique:users,email," . $id,
            "password" => "same:confirm-password",
            "roles" => "required"
        ]);

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = model::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));
        return redirect()->route('user.index');
    }


    public function destroy(Request $request, $id)
    {
        ActivityLog::where('user_id', $id)->delete();
        $user = model::find($request->id)->delete();
        return response()->json(['success' => true]);
    }
}
