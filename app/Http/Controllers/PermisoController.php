<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermisoController extends Controller
{
    function __construct()
    {
      $this->middleware('permission:ver-permiso|crear-permiso|editar-permiso|borrar-permiso', ['only' => ['index']]);
      $this->middleware('permission:crear-permiso', ['only' => ['create', 'store']]);
      $this->middleware('permission:editar-permiso', ['only' => ['edit', 'update']]);
      $this->middleware('permission:borrar-permiso', ['only' => ['destroy']]);
    }
   
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = Permission::all();
            return datatables()->of($items)->addColumn('action', function ($item) {
                return '<a href="' . route('permisos.edit', $item->id) . '" class="btn btn-info btn-sm"> Editar 
                           </a>&nbsp;&nbsp;<button type="button" name="delete" data-id="' . $item->id . '" class="delete btn btn-danger btn-sm"> Eliminar </button>';
            })
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('permisos.index');
    }

    public function create()
    {
        $permisos = Permission::get();
        return view('permisos.create', compact('permisos'));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required']);
        $permiso = Permission::create([
            'name' => $request->input('name'),
            'guard_name' => "web"
        ]);
        $permiso->save();

        return redirect()->route('permisos.index');
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $permiso = Permission::find($id);
 
        return view('permisos.edit', compact('permiso'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, ['name'=>'required']);

        $permiso = Permission::find($id);
        $permiso->name = $request->input('name');
        $permiso->save();
        return redirect()->route('permisos.index');
    }

    public function destroy($id)
    {
        $item = Permission::findOrFail($id);
        $item->delete();
        return response()->json(['success' => true]);
    }
}
