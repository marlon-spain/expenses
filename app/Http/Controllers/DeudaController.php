<?php

namespace App\Http\Controllers;

use App\Events\GraficoEvent;
use App\Models\Estado_deuda;
use Illuminate\Http\Request;
use App\Models\Proceso_deuda;
use App\Models\Deuda as model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;

class DeudaController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = model::all()
                ->where("total", "<", 100000);
            return datatables()->of($items)->addColumn('action', function ($item) {
                return '<a href="' . route('deuda.edit', $item->id) . '" class="btn btn-info btn-sm"> Editar 
                           </a>&nbsp;&nbsp;<button type="button" name="delete" data-id="' . $item->id . '" class="delete btn btn-danger btn-sm"> Eliminar </button>';
            })
                ->rawColumns(['action'])
                ->toJson();               
        }
       
        return view('deuda.index');
    }


    public function create()
    {
        $crear = model::all();
        $deuda = Estado_deuda::all();
        $proceso = Proceso_deuda::all();

        return view('deuda.create', compact('crear', 'deuda', 'proceso'));
    }


    public function store(Request $request)
    {
        $this->validate($request, ['fecha_inicio' => 'required', 'nombre' => 'required']);
        $user = Auth::user()->id;
        $store = model::create([
            'user_id' => $user,
            'fecha_inicio' => $request->input('fecha_inicio'),
            'nombre' => $request->input('nombre'),
            'fecha_fin' => $request->input('fecha_fin'),
            'total' => $request->input('total'),
            'restante' => $request->input('restante'),
            'estado_deuda_id' => $request->input('estado_deuda_id'),
            'Proceso_deuda_id' => $request->input('Proceso_deuda_id'),
        ]);
        $store->save();

        return redirect()->route('deuda.index');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {

        $edit   = model::find($id);
        $deuda  = Estado_deuda::all();
        $proceso = Proceso_deuda::all();
        return view('deuda.edit', compact('edit', 'deuda', 'proceso'));
    }


    public function update(Request $request, $id)
    {
        $update = model::find($id);
        $update->fecha_inicio = $request->input('fecha_inicio');
        $update->nombre = $request->input('nombre');
        $update->fecha_fin = $request->input('fecha_fin');
        $update->total = $request->input('total');
        $update->restante = $request->input('restante');
        $update->estado_deuda_id = $request->input('estado_deuda_id');
        $update->Proceso_deuda_id = $request->input('Proceso_deuda_id');
        $update->save();
        return redirect()->route('deuda.index');
    }


    public function destroy($id)
    {
        $item = model::findOrFail($id);
        $item->delete();
        return response()->json(['success' => true]);
    }
}
