<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\ActivityLog as model;

class ActivityLogController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('permission:ver-log', ['only' => ['index']]); //solo tenra permiso para ejecutar el metodo index
  }

  public function index(Request $request)
  {
    if ($request->ajax()) {
      $items = model::all();
      return datatables()->of($items)

        ->toJson();
    }
    return view('log.index');
  }

  public function destroy($id)
  {
    $user = model::find($id)->delete();
    return response()->json(['success' => true]);
  }
}
