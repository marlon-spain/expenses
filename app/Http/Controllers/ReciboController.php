<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Deuda;
use App\Models\Cuenta;
use App\Models\Nomina;
use App\Models\Tipo_pago;
use App\Models\Gasto_fijo;
use App\Models\Tipo_gasto;
use App\Models\ActivityLog;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Estado_recibo;
use App\Models\Recibo as model;
use App\Models\Seguimiento_recibo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ReciboController extends Controller
{

    public function index(Request $request)
    {
        $fecha = Carbon::now();

        $items = model::whereMonth('fecha', $fecha)->whereYear('fecha', $fecha)->get();
        $items1 = model::whereMonth('fecha', $fecha)->whereYear('fecha', $fecha)->where('tipo_gasto_id', '=', 1)->get();
        $items2 = model::whereMonth('fecha', $fecha)->whereYear('fecha', $fecha)->where('tipo_gasto_id', '=', 2)->get();

        //$totalmes1 = DB::select( DB::raw("SELECT * FROM recibos WHERE tipo_gasto_id = 1 AND MONTH(fecha) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH))"));                        
        //$totalmes2 =null ?? DB::select( DB::raw("SELECT * FROM recibos WHERE tipo_gasto_id = 1 AND MONTH(fecha) = MONTH(DATE_ADD(CURDATE(),INTERVAL -2 MONTH))")); 
        //$totalmes3 = null ?? DB::select(DB::raw("SELECT * FROM recibos WHERE tipo_gasto_id = 1 AND MONTH(fecha) = MONTH(DATE_ADD(CURDATE(),INTERVAL -3 MONTH))"));

        if ($request->ajax()) {
            $items = model::whereMonth('fecha', $fecha)->whereYear('fecha', $fecha)->get();
            return datatables()->of($items)->addColumn('action', function ($item) {
                return '<a href="' . route('recibo.show', $item->id) . '" class="fa fa-info-circle"> 
                </a>&nbsp;&nbsp;<a href="' . route('recibo.edit', $item->id) . '" class="btn btn-info btn-sm"> Editar 
                           </a>&nbsp;&nbsp;<button type="button" name="delete" data-id="' . $item->id . '" class="delete btn btn-danger btn-sm"> Eliminar </button>';
            })
                ->rawColumns(['action'])
                ->toJson();
        }

        return view('recibo.index');
    }


    public function create()
    {
        $crear = model::all();
        $cuenta = Cuenta::all();
        $tipo_gasto = Tipo_gasto::all();
        $gasto_fijo = Gasto_fijo::all();
        $estado = Estado_recibo::all();
        $tipo_pago = Tipo_pago::all();
        $nomina = Nomina::all();
        $deuda = Deuda::where('restante', '!=', 0)->get();

        return view('recibo.create', compact('crear', 'cuenta', 'tipo_gasto', 'gasto_fijo', 'estado', 'tipo_pago', 'nomina', 'deuda'));
    }


    public function store(Request $request)
    {
        // $this->validate($request, []);
        $date = Carbon::now();
        $nomina = Nomina::all()->last();

        $nomina = $nomina->id;
        $date = $date->format('y-m-d');
        $user = Auth::user()->id;
        $store = model::create([
            'user_id' => $user,
            'fecha' => $request->input('fecha'),
            'descripcion' => $request->input('descripcion'),
            'importe' => $request->input('importe'),
            'cuenta_id' => $request->input('cuenta_id'),
            'tipo_gasto_id' => $request->input('tipo_gasto_id'),
            'gasto_fijo_id' => $request->input('gasto_fijo_id'),
            'estado_id' => 1, //siempre pagada
            'tipo_pago_id' => 1,
            'nomina_id' => $nomina, //siempre el mes actual
            'deuda_id' => $request->input('deuda_id'),
        ]);

        //  return $store;                                                       
        $store->save();

        //crear seguimiento
        $item = Nomina::find($store->nomina_id);
        if (strpos($store->descripcion,  'recibida') !== false) { //si encuentra la palabra ingreso devolvera true y se ingresa  
            $item->update([
                'restante' => $request->importe + $item->restante,
            ]);
        } else {
            $item->update([
                'restante' => $item->restante - $request->importe,

            ]);
        }

        //crear seguimiento
        $seguimiento = Seguimiento_recibo::create([
            'user_id' => $store->user_id,
            'recibo_id' => $store->id,
            'estado_recibo_id' => $store->estado_id,
            'comentarios' => $store->importe,
        ]);

        $store->$seguimiento;



        return redirect()->route('recibo.index');
    }


    public function show($id)
    {
        $item = model::find($id);

        $query = model::where('user_id', Auth::user()->id)
            ->where('descripcion', 'LIKE', '%' . $item->descripcion . '%')
            ->orderBy('fecha','desc')
            ->pluck('importe', 'fecha')
            ->take(12); 
            
        $importes = [];
        $totalImportes = 0; // Variable para el conteo de importes
        $contador = 0; // Variable para contar los importes
        $valorMasAlto = null; // Variable para el valor más alto
        $valorMasBajo = null; // Variable para el valor más bajo
        foreach ($query as $fecha => $importe) {

            $importes[] = [
                'importe' => $importe,
                'fecha' => date('F Y', strtotime($fecha))
            ];
            $totalImportes += $importe; // Sumar el importe al total
            $contador++; // Incrementar el contador de importes

            if ($valorMasAlto === null || $importe > $valorMasAlto) {
                $valorMasAlto = $importe; // Actualizar el valor más alto
            }
        
            if ($valorMasBajo === null || $importe < $valorMasBajo) {
                $valorMasBajo = $importe; // Actualizar el valor más bajo
            }
        }
        
        $media =["alto"=> $valorMasAlto, "bajo"=>$valorMasBajo]; // Calcular la media
      

        return view('recibo.detail', compact('item', 'importes', 'totalImportes','media'));
    }


    public function edit($id)
    {
        //  $this->authorize('author', $id);

        $edit = model::find($id);
        $cuenta = Cuenta::all();
        $tipo_gasto = Tipo_gasto::all();
        $gasto_fijo = Gasto_fijo::all();
        $estado = Estado_recibo::all();
        $tipo_pago = Tipo_pago::all();
        $nomina = Nomina::all();
        $deuda = Deuda::all();
        return view('recibo.edit', compact('edit', 'cuenta', 'tipo_gasto', 'gasto_fijo', 'estado', 'tipo_pago', 'nomina', 'deuda'));
    }


    public function update(Request $request, $id)
    {
        // $this->authorize('author', $id);

        $update = model::find($id);

        $update->fecha = $request->input('fecha');
        $update->descripcion = $request->input('descripcion');
        $update->importe = $request->input('importe');
        $update->cuenta_id = $request->input('cuenta_id');
        $update->tipo_gasto_id = $request->input('tipo_gasto_id');
        $update->gasto_fijo_id = $request->input('gasto_fijo_id');
        $update->estado_id = $request->input('estado_id');
        $update->tipo_pago_id = $request->input('tipo_pago_id');
        $update->nomina_id = $request->input('nomina_id');
        // $update->deuda_id = $request->input('deuda_id');

        $update->save();


        //crear seguimiento
        $seguimiento = Seguimiento_recibo::create([
            'user_id' => $update->user_id,
            'recibo_id' => $update->id,
            'estado_recibo_id' => $update->estado_id,
            'comentarios' => $update->descripcion,
        ]);

        $update->$seguimiento;

        return redirect()->route('recibo.index');
    }

    public function destroy($id)
    {
        // $this->authorize('author', $id);

        $item = model::findOrFail($id);
        $item->delete();
        return response()->json(['success' => true]);
    }
}
