<?php

namespace App\Http\Controllers;

use App\Models\Recibo;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Nomina as model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;


class NominaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:ver-nomina|crear-nomina|editar-nomina|borrar-nomina', ['only' => ['index']]); //solo tenra permiso para ejecutar el metodo index
        $this->middleware('permission:crear-nomina', ['only' => ['create', 'store']]); //solo tenra permiso para ejecutar el metodo crear
        $this->middleware('permission:editar-nomina', ['only' => ['edit', 'update']]); //solo tenra permiso para ejecutar el metodo editar
        $this->middleware('permission:borrar-nomina', ['only' => ['destroy']]); //solo tenra permiso para ejecutar el metodo destroy
    }

    public function index(Request $request)
    {
      
        $fecha = Carbon::now();
        if ($request->ajax()) {
            $items = model::all();
            // $items = model::whereYear('fecha', $fecha)->get();

            return datatables()->of($items)->addColumn('action', function ($item) {
                return '<a href="' . route('nomina.edit', $item->id) . '" class="btn btn-info btn-sm"> Editar 
                       </a>&nbsp;&nbsp;<button type="button" name="delete" data-id="' . $item->id . '" class="delete btn btn-danger btn-sm"> Eliminar </button>';
            })
                ->rawColumns(['action'])
                ->toJson();
        }
        // return $items;exit;
        return view('nomina.index');
    }


    public function create()
    {
        $crear = model::get();
        $empresa = Empresa::all();
        return view('nomina.create', compact('crear', 'empresa'));
    }


    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'fecha' => 'required',
                'devengo' => 'required',
                'deducciones' => 'required',
                'IRPF' => 'required',
                'remuneracion' => 'required',
            ]
        );
        $user = Auth::user()->id;
        $store = model::create([
            'user_id' => $user,
            'fecha' => $request->input('fecha'),
            'devengo' => $request->input('devengo'),
            'deducciones' => $request->input('deducciones'),
            'IRPF' => $request->input('IRPF'),
            'remuneracion' => $request->input('remuneracion'),
            'restante' => $request->remuneracion,
            'empresa_id' => $request->input('empresa_id'),
            'comentarios' => $request->input('comentarios')
        ]);
        $store->save();
        return redirect()->route('nomina.index')->with('success', 'Los cambios se han creado correctamente.');
    }


    public function show($id)
    {
        $nomina = model::findOrFail($id);
        return view('nomina.detail', compact('nomina'));
    }

    public function edit($id)
    {
        $edit   = model::find($id);
        if (Gate::denies('delete', $edit)) {
            abort(403, 'No estás autorizado para eliminar este elemento.');
        }
        $empresa = Empresa::all();
        return view('nomina.create', compact('edit', 'empresa'));
    }


    public function update(Request $request, $id)
    {
        $update = model::find($id);

        $update->fecha = $request->input('fecha');
        $update->devengo = $request->input('devengo');
        $update->deducciones = $request->input('deducciones');
        $update->IRPF = $request->input('IRPF');
        $update->remuneracion = $request->input('remuneracion');
        $update->restante = $request->input('restante');
        $update->comentarios = $request->input('comentarios');
        $update->empresa_id = $request->input('empresa_id');
        $update->save();

        return redirect()->route('nomina.index')->with('success', 'Los cambios se guardaron correctamente.');
    }


    public function destroy($id)
    {        
        $item = model::findOrFail($id);
        if (Gate::denies('delete', $item)) {
            return response()->json(['success' => false, 'message' => 'No estás autorizado para eliminar este elemento.']);
        }
        $item->delete();
        return response()->json(['success' => true]);
    }
}
