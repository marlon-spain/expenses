<?php

namespace App\Http\Controllers\Dashboard;

use DateTime;
use Carbon\Carbon;
use App\Models\Dashboard;
use Illuminate\Support\Arr;
use App\Models\BudgetStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\Dashboard\DashboardRepository;


class DashboardController extends Controller
{
    public function __construct(
        private DashboardRepository $dashboardRepository
    )
    {
    }

    public function getBudgetQuery(Request $request)
    {
        $allowed = [
            'fromDate' => 'required|date_format:Y-m-d',
            'toDate' => 'required|date_format:Y-m-d',
        ];
        $errors = $this->validatesRequests($request, $allowed);
        if (!empty($errors)) return $this->errorResponseJson($errors);

        $dashboard = new Dashboard();
        $query = $dashboard->setTable('budget');

        if ($request->has('fromDate') and $request->has('toDate')) {
            if ($request->fromDate == $request->toDate) {
                $query->setSelectTotalHour();
            } else {
                $query->setSelectTotalDate();
            }
            $currentDateTime = Carbon::createFromFormat('Y-m-d', $request->toDate);
            $request->toDate = $currentDateTime->format('Y-m-d');
            $query->query->whereBetween('budget' . '.created_at', [$request->fromDate, $request->toDate]);
        }

        if ($request->get('budget_status_id') !== null) $query->setWhere(['budget_status_id', '=', $request->budget_status_id ?? null]);
        if ($request->get('centers')) $query->query->whereIn('center_id', $request->centers);

        $query->query->groupBy('date');
        $response = $query->queryBuild();
        return  $query->setReturnTotal($response);
    }

    public function getImportQuery(Request $request)
    {
        $allowed = [
            'fromDate' => 'required|date_format:Y-m-d',
            'toDate' => 'required|date_format:Y-m-d',
        ];
        $errors = $this->validatesRequests($request, $allowed);
        if (!empty($errors)) return $this->errorResponseJson($errors);

        $dashboard = new Dashboard();
        $query = $dashboard->setTable('budget');

        if ($request->has('fromDate') and $request->has('toDate')) {
            if ($request->fromDate == $request->toDate) {
                $query->setSelectTotalHour();
            } else {
                $query->setSelectTotal();
            }
            $currentDateTime = Carbon::createFromFormat('Y-m-d', $request->toDate);
            $request->toDate = $currentDateTime->format('Y-m-d');
            $query->query->whereBetween('budget' . '.created_at', [$request->fromDate, $request->toDate]);
        }

        if ($request->get('budget_status_id') !== null) $query->setWhere(['budget_status_id', '=', $request->budget_status_id ?? null]);
        if ($request->get('centers')) $query->query->whereIn('center_id', $request->centers);

        $query->query->selectRaw('sum(net_amount) as importe');
        $response = $query->queryBuild();
        foreach ($response as $importe);
        return $importe;
    }

    //Centros más rentables en presupuestos (total, + importe)
    public function getProfitableCenters(Request $request)
    {        
        $currentDateTime = Carbon::createFromFormat('Y-m-d', $request->toDate);
        $request->toDate = $currentDateTime->format('Y-m-d');        
        $allowed = [
            'fromDate' => 'required|date_format:Y-m-d',
            'toDate' => 'required|date_format:Y-m-d',
        ];
        $errors = $this->validatesRequests($request, $allowed);
        if (!empty($errors)) return $this->errorResponseJson($errors);
       
        $query = $this->dashboardRepository->setTable('budget');             
        $query->query->selectRaw('center_id as center, COUNT(*) as totalPeticiones, SUM(net_amount) as Importe')
                     ->whereBetween('budget' . '.created_at', [$request->fromDate, $request->toDate])
                     ->groupBy('center_id')
                     ->orderBy('totalPeticiones','DESC')->take(5)->get();
        $response = $query->queryBuild();       
        foreach ($response as $importe);
        return $response;
    }

    //Centros más rentables en presupuestos (total, + importe) pruebas mas presupuestadas centro 
    public function getProfitableTest(Request $request)
    {
        $currentDateTime = Carbon::createFromFormat('Y-m-d', $request->toDate);
        $request->toDate = $currentDateTime->format('Y-m-d');

        $allowed = [
            'fromDate' => 'required|date_format:Y-m-d',
            'toDate' => 'required|date_format:Y-m-d',
        ];
        $errors = $this->validatesRequests($request, $allowed);
        if (!empty($errors)) return $this->errorResponseJson($errors);
   
        $query = $this->dashboardRepository->setTable('budget_items');             
        $query->query->selectRaw('COUNT(test_id) as totalPeticiones, SUM(budget_items.net_amount) as Importe,test_id as test')
                     ->whereBetween('budget' . '.created_at', [$request->fromDate, $request->toDate]);
                     $query->query->join('budget', function ($join) {
                        $join->on('budget_items.budget_id', '=', 'budget.id');
                    }); 
         if ($request->get('centers')) $query->query->whereIn('center_id', $request->centers);
        $query->query->groupBy('test_id')
                     ->orderBy('totalPeticiones','DESC')->take(25)->get();
        $response = $query->queryBuild();   
        return  $query->setReturnTotalTest($response);
       // foreach ($response as $importe){
      //  $test = (new MdmConnectionService())->findTest($importe->test);        
     //   $importe->test = $test;    
      //  }
      //  return $response;
    }

    public function getRefusalTest(Request $request)
    {
        $currentDateTime = Carbon::createFromFormat('Y-m-d', $request->toDate);
        $request->toDate = $currentDateTime->format('Y-m-d');

        $allowed = [
            'fromDate' => 'required|date_format:Y-m-d',
            'toDate' => 'required|date_format:Y-m-d',
        ];
        $errors = $this->validatesRequests($request, $allowed);
        if (!empty($errors)) return $this->errorResponseJson($errors);
   
        $query = $this->dashboardRepository->setTable('budget_items');             
        $query->query->selectRaw('COUNT(test_id) as totalPeticiones, SUM(budget_items.net_amount) as Importe,test_id as test')
                     ->whereBetween('budget' . '.created_at', [$request->fromDate, $request->toDate])
                     ->where('budget_status_id', '=',3);
                     $query->query->join('budget', function ($join) {
                        $join->on('budget_items.budget_id', '=', 'budget.id');
                    }); 
        if ($request->get('centers')) $query->query->whereIn('center_id', $request->centers);
        $query->query->groupBy('test_id')
                     ->orderBy('totalPeticiones','DESC');
        $response = $query->queryBuild();   
        return  $query->setReturnTotalTest($response);
    }

    //ratio de aceptacion budget
    public function getRatioBudget(Request $request)
    {
        $currentDateTime = Carbon::createFromFormat('Y-m-d', $request->toDate);
        $request->toDate = $currentDateTime->format('Y-m-d');

        $allowed = [
            'fromDate' => 'required|date_format:Y-m-d',
            'toDate' => 'required|date_format:Y-m-d',
        ];
        $errors = $this->validatesRequests($request, $allowed);
        if (!empty($errors)) return $this->errorResponseJson($errors);
   
        $query = $this->dashboardRepository->setTable('budget_items');             
        $query->query->selectRaw('COUNT(test_id) as totalPeticiones, SUM(budget_items.net_amount) as Importe,test_id as test')
                     ->whereBetween('budget' . '.created_at', [$request->fromDate, $request->toDate]);
                     $query->query->join('budget', function ($join) {
                        $join->on('budget_items.budget_id', '=', 'budget.id');
                    }); 
        if ($request->get('centers')) $query->query->whereIn('center_id', $request->centers);
        $query->query->groupBy('test_id')
                     ->orderBy('totalPeticiones','DESC')->take(25)->get();
        $response = $query->queryBuild();   
        return  $query->setReturnTotalTest($response);
    }

    //ratio de aceptacion test
    public function getRatioTest(Request $request)
    {
        $currentDateTime = Carbon::createFromFormat('Y-m-d', $request->toDate);
        $request->toDate = $currentDateTime->format('Y-m-d');

        $allowed = [
            'fromDate' => 'required|date_format:Y-m-d',
            'toDate' => 'required|date_format:Y-m-d',
        ];
        $errors = $this->validatesRequests($request, $allowed);
        if (!empty($errors)) return $this->errorResponseJson($errors);
   
        $query = $this->dashboardRepository->setTable('budget_items');             
        $query->query->selectRaw('COUNT(test_id) as totalPeticiones, SUM(budget_items.net_amount) as Importe,test_id as test')
                     ->whereBetween('budget' . '.created_at', [$request->fromDate, $request->toDate]);
                     $query->query->join('budget', function ($join) {
                        $join->on('budget_items.budget_id', '=', 'budget.id');
                    }); 
        if ($request->get('centers')) $query->query->whereIn('center_id', $request->centers);
        $query->query->groupBy('test_id')
                     ->orderBy('totalPeticiones','DESC')->take(25)->get();
        $response = $query->queryBuild();   
        return  $query->setReturnTotalRatioTest($response);
    }

    

    /*
Presupuestos generados (+importe) ok 
Presupuestos procesados (+importe) ok
Presupuestos cancelados (+importe) ok
Presupuestos rechazados (+importe) ok

Centros más rentables en presupuestos (total, + importe) ok un top 10 ok

Pruebas más presupuestadas (total, + importe)  + si pasa los centros [] todo los centros con sus importes si pasa uno solo los importes de ese centro  top 10

Pruebas más rechazadas (total, +importe)

Motivos de no venta

Ratio de aceptación

Ratio de aceptación por prueba

*/

    //la restricciones del config/database -> 'strict' => false,

}
