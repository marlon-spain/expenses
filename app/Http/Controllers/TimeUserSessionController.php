<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TimeUserSession as model;
use Yajra\DataTables\Facades\DataTables;
use synlab\synlab_ratings\Models\MainModel;

class TimeUserSessionController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = model::all();
            return datatables()->of($items)
              
                ->toJson();
        }
        return view('timeUserSession.index');
    }
}
