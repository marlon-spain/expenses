<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empresa as model;
use Yajra\DataTables\Facades\DataTables;

class EmpresaController extends Controller
{

    public function index(Request $request)
    {
        //https://www.youtube.com/watch?v=2P5f6RBfxYk
        if ($request->ajax()) {
            $items = model::all()
                ->where("total", "<", 100000);
            return datatables()->of($items)->addColumn('action', function ($item) {
                return '<a href="' . route('empresa.edit', $item->id) . '" class="btn btn-info btn-sm"> Editar 
                           </a>&nbsp;&nbsp;<button type="button" name="delete" data-id="' . $item->id . '" class="delete btn btn-danger btn-sm"> Eliminar </button>';
            })
                ->rawColumns(['action'])
                ->toJson();               
        }


        return view('empresa.index');
    }

    //no exoxste create porque se hace en la misma pagina del index
    public function store(Request $request)
    {

        $store = model::create([
            'nombre' => $request->input('nombre')
        ]);

        // return $store; exit;
        $store->save();

        return back();
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $item = model::findOrFail($id);
        $item->delete();
        return response()->json(['success' => true]);
    }
}
