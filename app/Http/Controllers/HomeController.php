<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->user()->hasRole('Super-Admin')) {
            return redirect()->route('user.index');
        } else if ($request->user()->hasRole('User')) {
            return redirect()->route('kpis.action');
        } else {
            return redirect()->route('profile.show');
        }
    }
}
