<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Deuda;
use App\Models\Nomina;
use App\Models\Recibo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Dashboard\KpisRepository;
use App\Repositories\Dashboard\LogsRepository;
use App\Repositories\Dashboard\DashboardRepository;

class KpisController extends Controller
{
    public function __construct(
        private DashboardRepository $dashboardRepository,
        private KpisRepository $kpisRepository,
        private LogsRepository $logsRepository,
        private Recibo $recibo,
        private Nomina $nomina,
        private Deuda $deuda
    ) {
    }



    public function getTotalrecibo(Request $request)
    {
        $query = $this->dashboardRepository->setTable('recibos');
        $recibo = $this->recibo->selectRaw('COUNT(*) as totalRecibos, SUM(importe) as Importe');

        $query->query->selectRaw('COUNT(id) as totalRecibos, SUM(importe) as Importe');
        $item = $query->queryBuild()->first();      

        $query->query->selectRaw('COUNT(*) as totalRecibos, SUM(importe) as Importe')
            ->where('estado_id', '=', 1);
        $pagado = $query->queryBuild()->first();


        $devuelto = $this->recibo->selectRaw('COUNT(*) as totalRecibos, SUM(importe) as Importe')
            ->where('estado_id', '=', 2)->first();

        $impagado = $this->recibo->selectRaw('COUNT(*) as totalRecibos, SUM(importe) as Importe')
            ->where('estado_id', '=', 3)->first();

        $cancelado = $this->recibo->selectRaw('COUNT(*) as totalRecibos, SUM(importe) as Importe')
            ->where('estado_id', '=', 6)->first();

        $nomina = $this->nomina->selectRaw('COUNT(*) as totalNominas, SUM(remuneracion) as Importe')
            ->first();

        $topRecibos = $this->recibo->selectRaw('estado_id, COUNT(*) as totalRecibos, SUM(importe) as Importe')
            ->groupBy('estado_id')
            ->orderBy('totalRecibos', 'DESC')->take(5)->get();

        $topNominas = $this->nomina->selectRaw('fecha, COUNT(*) as totalNominas, SUM(remuneracion) as Importe')
            ->whereBetween('remuneracion', ['100', '2000'])
            ->where('fecha', '>=', Carbon::now()->subYears(1)) //año actual
            ->groupBy('fecha')
            ->orderBy('fecha', 'DESC')->take(5)->get();

        $topGastos = $this->recibo->selectRaw('gasto_fijo_id, COUNT(*) as totalGastos, SUM(importe) as Importe')
            ->where('created_at', '>=', Carbon::now()->subYears(1)) //año actual
            ->groupBy('gasto_fijo_id')
            ->orderBy('totalGastos', 'DESC')->take(5)->get();

        $topPagos = $this->recibo->selectRaw('tipo_pago_id, COUNT(*) as totalPagos, SUM(importe) as Importe')
            ->where('created_at', '>=', Carbon::now()->subYears(1)) //año actual
            ->groupBy('tipo_pago_id')
            ->orderBy('totalPagos', 'DESC')->take(5)->get();

        $topdeudas = $this->deuda->selectRaw('*')
            ->where('estado_deuda_id', '=', 1)
            ->orWhere('estado_deuda_id', '=', 2)
            ->orderBy('estado_deuda_id', 'ASC')->take(5)->get();

        $topEstados = $this->recibo->selectRaw('estado_id, COUNT(*) as totalEstados, SUM(importe) as Importe')
            ->groupBy('estado_id')
            ->orderBy('totalEstados', 'DESC')->take(5)->get();

        $grafico = $this->kpisRepository->recibos();
        $grafico2 = $this->kpisRepository->topRecibos();
        $grafico3 = $this->kpisRepository->topGastos();
        $grafico4 = $this->kpisRepository->logs();

        //logs
        $log = $this->logsRepository->logs();
        $log2 = $this->logsRepository->logsGet();
        $log3 = $this->logsRepository->logsPost();
        $log4 = $this->logsRepository->logsPut();
        $log5 = $this->logsRepository->logsPatch();
        $log6 = $this->logsRepository->logsDelete();
        $log7 = $this->logsRepository->logsUser();
        $log8 = $this->logsRepository->logsRole();
        $log9 = $this->logsRepository->logsPermission();
        $log10 = $this->logsRepository->topMethod();
        $log11 = $this->logsRepository->topRoute();
        $log12 = $this->logsRepository->topAlias();
        $log13 = $this->logsRepository->topIp();
        $log14 = $this->logsRepository->topAgent();
        $log15 = $this->logsRepository->topUser();

        //return $log14;
        //return $grafico4;

        return view('kpis.index', [
            "item" => $item,
            "pagado" => $pagado,
            "devuelto" => $devuelto,
            "impagado" => $impagado,
            "cancelado" => $cancelado,
            "nomina" => $nomina,
            "topRecibos" => $topRecibos,
            "topNominas" => $topNominas,
            "topGastos" => $topGastos,
            "topPagos" => $topPagos,
            "topdeudas" => $topdeudas,
            "topEstados" => $topEstados,
            "grafico" => json_encode($grafico),
            "grafico2" => json_encode($grafico2),
            "grafico3" => json_encode($grafico3),
            "grafico4" => json_encode($grafico4),
            "log" => $log,
            "log2" => $log2,
            "log3" => $log3,
            "log4" => $log4,
            "log5" => $log5,
            "log6" => $log6,
            "log7" => $log7,
            "log8" => $log8,
            "log9" => $log9,
            "log10" => $log10,
            "log11" => $log11,
            "log12" => $log12,
            "log13" => $log13,
            "log14" => $log14,
            "log15" => $log15
        ]);
    }


    public function search()
    {
        $fecha = Carbon::now();
        $items = Recibo::query();


        $items->whereMonth('fecha', $fecha)
            ->where('tipo_gasto_id', 1);


        $items = $items->orderBy('fecha', 'asc')
            ->take(12)
            ->get();

        $data = $items->map(function ($item) {
            return [
                'name' => $item->descripcion,
                'fecha' => $item->fecha,
                'y' => floatval($item->importe),
            ];
        })->all();

        return view('kpis.search')->with('data', $data);
    }

    public function searchFilter(Request $request)
    {
        $fecha = Carbon::now();
        $items = Recibo::query();
        $search = $request->input('search');

        $items->where('descripcion', 'LIKE', '%' . $search . '%')
            ->orWhere('importe', 'LIKE', '%' . $search . '%');


        $items = $items->orderBy('fecha', 'asc')
            ->take(12)
            ->get();

        $data = $items->map(function ($item) {
            return [
                'name' => $item->descripcion,
                'fecha' => $item->fecha,
                'y' => floatval($item->importe),
            ];
        })->all();
        return $data;
    }

    public function action(Request $request)
    {
        $user = Auth::user();
        //contar los logs
        $user->logCount = $user->logs()->count();
        $user->reciboCount = $user->recibos()->count();
        $user->deudaCount = $user->deudas()->count();
        $user->timeSessionMedia = gmdate('H:i:s', $user->timeUserSession()->avg('time'));
        $user->nominaMedia = number_format($user->nominas()->avg('remuneracion'), 0, '', ',');


        //El porcentaje de deducciones en relación al devengo en las nóminas del usuario.
        $mediaDevengo = $user->nominas->avg('devengo');
        $mediaDeducciones = $user->nominas->avg('deducciones');
        $user->porcentajeDeducciones = ($mediaDevengo > 0) ? ($mediaDeducciones / $mediaDevengo) * 100 : 0;
        $user->porcentajeDeducciones = number_format($user->porcentajeDeducciones, 2);

        //cantidad de nomina por año
        $fechaInicio = '2023-01-01';
        $fechaFin = '2023-12-31';

        $totalNominaAnual = $user->nominas()
            ->whereBetween('fecha', [$fechaInicio, $fechaFin])
            ->sum('devengo');

        $user->totalNominaAnual = number_format($totalNominaAnual,  0, '', ',');


        //ratio de uso de cuentas
        $cuentaReciboCounts = $user->recibos->pluck('cuenta_id')->countBy();
        $totalRecibos = $cuentaReciboCounts->sum();
        $ratiosUso = [];
        foreach ($cuentaReciboCounts as $cuentaId => $reciboCount) {
            $ratiosUso[] = ($totalRecibos > 0) ? ($reciboCount / $totalRecibos) * 100 : 0;
        }
        $user->ratiosUsoCuentas = $ratiosUso;

        unset($user->nominas);
        unset($user->recibos);

        //return $user;
        return view('kpis.action', compact('user'));
    }

    public function inicio(Request $request)
    {    
        
        $influx = $this->kpisRepository->getInfluxByUser(Auth::user()->id);
  
        $typeWeek = $this->kpisRepository->getRequestWeekTypeParam(Auth::user()->id, 'user_id');
        //return $typeWeek;
        return view('inicio.index', compact('influx', 'typeWeek'));
    }
}
