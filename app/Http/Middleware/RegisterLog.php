<?php

namespace App\Http\Middleware;

use App\Models\ActivityLog;
use Closure;
use Illuminate\Http\Request;

class RegisterLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $authUser = $request->user();
        $activity = [
            "user_id" => $authUser->id ?? null,
         //   "user_permissions" => $authUser != null && $authUser->permissions !== null ? $authUser->permissions->toArray() : null,
            "route_path" => $request->path()  ,
            "route_method" => $request->getMethod(),
            "route_alias" => (empty($request->route()->getName())) ? $request->route()->getPrefix() : $request->route()->getName(),
        //    "request_headers" => json_encode($request->header()),
            "user_agent" => $request->header('User-Agent') ?? 'Not defined',
            "ip_address" => $request->ip(),
            "is_proxy" => $request->isFromTrustedProxy()
        ];
        ActivityLog::storeActivity($activity);
        return $next($request);
    }
}
