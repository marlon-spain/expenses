<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Nomina;
use App\Models\Recibo;
use App\Models\Seguimiento_recibo;
use Illuminate\Console\Command;

class RegisteRecibo extends Command
{

    protected $signature = 'recibo:task';


    protected $description = 'crea recibos automaticamente mensual';


    public function handle()
    {
        $firstDayMonth = Carbon::now()->firstOfMonth()->format('Y-m-d');
        $dayMont = Carbon::now()->firstOfMonth()->addDays(2)->startOfDay();
        $now = Carbon::now()->format('Y-m-d');
        $nomina = Nomina::where('fecha', $firstDayMonth)->first();

        $request = [
            'user_id' => 2,
            'fecha' => $firstDayMonth,
            'descripcion' => "Cargo cuota de Hipoteca ING Direct",
            'importe' => 465,20,
            'cuenta_id' => 2,
            'tipo_gasto_id' => 1,
            'gasto_fijo_id' => 1,
            'estado_id' => 1,
            'tipo_pago_id' => 1,
            'nomina_id' => $nomina['id'] ?? null, //siempre el mes actual
            'deuda_id' => 1,
        ];
        $recibo = Recibo::create($request);

        //crear seguimiento
        $seguimiento = Seguimiento_recibo::create([
            'user_id' => $recibo->user_id,
            'recibo_id' => $recibo->id,
            'estado_recibo_id' => $recibo->estado_id,
            'comentarios' => $recibo->descripcion,
        ]);

        //crear traspaso cuenta
        $request2 = [
            'user_id' => 2,
            'fecha' => $firstDayMonth,
            'descripcion' => "Traspaso periódico emitido nómina SPO To 2051036876",
            'importe' => 40,00,
            'cuenta_id' => 2,
            'tipo_gasto_id' => 2,
            'gasto_fijo_id' => 4,
            'estado_id' => 1,
            'tipo_pago_id' => 1,
            'nomina_id' => $nomina['id'] ?? null, //siempre el mes actual
            'deuda_id' => null,
        ];
        $recibo2 = Recibo::create($request2);

        //crear seguimiento
        $seguimiento2 = Seguimiento_recibo::create([
            'user_id' => $recibo2->user_id,
            'recibo_id' => $recibo2->id,
            'estado_recibo_id' => $recibo2->estado_id,
            'comentarios' => $recibo2->descripcion,
        ]);

        //deuda ing
        $request3 = [
            'user_id' => 2,
            'fecha' => $dayMont,
            'descripcion' => "Cargo cuota de préstamo",
            'importe' => 133,00,
            'cuenta_id' => 2,
            'tipo_gasto_id' => 2,
            'gasto_fijo_id' => 4,
            'estado_id' => 1,
            'tipo_pago_id' => 1,
            'nomina_id' => $nomina['id'] ?? null, //siempre el mes actual
            'deuda_id' => 10       
        ];
        $recibo3 = Recibo::create($request3);

        //crear seguimiento
        $seguimiento3 = Seguimiento_recibo::create([
            'user_id' => $recibo3->user_id,
            'recibo_id' => $recibo3->id,
            'estado_recibo_id' => $recibo3->estado_id,
            'comentarios' => $recibo3->descripcion         
        ]);

           //orange ing
           $request3 = [
            'user_id' => 2,
            'fecha' => $dayMont,
            'descripcion' => "Recibo RSG GROUP ESPANA SL",
            'importe' => 29,90,
            'cuenta_id' => 2,
            'tipo_gasto_id' => 1,
            'gasto_fijo_id' => 29,
            'estado_id' => 1,
            'tipo_pago_id' => 1,
            'nomina_id' => $nomina['id'] ?? null, //siempre el mes actual
            'deuda_id' => null      
        ];
        $recibo3 = Recibo::create($request3);

        //crear seguimiento
        $seguimiento3 = Seguimiento_recibo::create([
            'user_id' => $recibo3->user_id,
            'recibo_id' => $recibo3->id,
            'estado_recibo_id' => $recibo3->estado_id,
            'comentarios' => $recibo3->descripcion         
        ]);
    }
}
