<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Nomina;
use Illuminate\Console\Command;

class RegisterNomina extends Command
{

    protected $signature = 'nomina:task';

    protected $description = 'crea nomina automaticamente mensualmente';


    public function handle()
    {
        $now = Carbon::now()->format('Y-m-d');
        $firstDayMonth = Carbon::now()->firstOfMonth()->format('Y-m-d');
        $comentarios= 'Nomina del mes de '.Carbon::now()->format('M');

        $request = [
            'user_id' => 2,
            'fecha' => $firstDayMonth,
            'devengo' => 1600.49,
            'deducciones' => 320.66,
            'IRPF' => 12,
            'remuneracion' => 1300.00,
            'restante' => 1300.00,
            'empresa_id' => 1,
            'comentarios' => $comentarios
        ];
        Nomina::create($request);
    }
}
