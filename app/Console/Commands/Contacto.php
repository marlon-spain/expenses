<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class Contacto extends Command
{
    
    protected $signature = 'test:task';

    
    protected $description = 'nueva tarea cron jobs';

    
    public function handle()
    {
        $texto ="[" .date("Y-m-d H:i:s"). "]: hola esta tarea es personalizada marlon briones";
        Storage::append("archivo.txt", $texto);
    }
}
