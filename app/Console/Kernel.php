<?php

namespace App\Console;

use App\Console\Commands\RegisteRecibo;
use App\Console\Commands\RegisterNomina;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    protected $commands = [
        Commands\Contacto::class,
        RegisteRecibo::class,
        RegisterNomina::class
  ];
    
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('test:task')->everyMinute();
        $schedule->command('nomina:task')->monthly();
        $schedule->command('recibo:task')->monthly();
    }

    

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
