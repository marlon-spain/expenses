<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\RuleExecution;

class RuleExecutionRepository extends BaseRepository
{
    protected function model(): string
    {
        return RuleExecution::class;
    }
}
