<?php

declare(strict_types=1);

namespace App\Repositories\Dashboard;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Recibo;
use App\Models\ActivityLog;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Models\Permission;

class LogsRepository extends BaseRepository
{
  public  ?object $query = null;
  protected  ?string $fromDate = null;
  protected  ?string $toDate = null;
  protected  ?string $table = null;
  protected  array $where = [];
  protected  ?string $activeFilterAuto = null;

  public function __construct(
    private ActivityLog $log,
    private User $user,
    private Role $role,
    private Permission $permission,
  ) {
  }

  public function logs()
  {
    $items = $this->log->selectRaw('COUNT(*) as totalLogs')->get();
    foreach ($items as $item);
    return $item;
  }

  public function logsGet()
  {
    $items = $this->log->selectRaw('route_method, COUNT(*) as totalLogs')
      ->where('route_method', '=', 'GET')
      ->groupBy('route_method')
      ->get();
    foreach ($items as $item);
    return $item;
  }

  public function logsPost()
  {
    $items = $this->log->selectRaw('route_method, COUNT(*) as totalLogs')
      ->where('route_method', '=', 'POST')
      ->groupBy('route_method')
      ->get();
    foreach ($items as $item);
    return $item;
  }

  public function logsPut()
  {
    $items = $this->log->selectRaw('route_method, COUNT(*) as totalLogs')
      ->where('route_method', '=', 'PUT')
      ->groupBy('route_method')
      ->get();
    foreach ($items as $item);
    return $item;
  }

  public function logsPatch()
  {
    $items = $this->log->selectRaw('route_method, COUNT(*) as totalLogs')
      ->where('route_method', '=', 'PATCH')
      ->groupBy('route_method')
      ->get();
    foreach ($items as $item);
    return $item;
  }

  public function logsDelete()
  {
    $items = $this->log->selectRaw('route_method, COUNT(*) as totalLogs')
      ->where('route_method', '=', 'DELETE')
      ->groupBy('route_method')
      ->get();
    foreach ($items as $item);
    return $item;
  }

  public function logsUser()
  {
    $items = $this->user->selectRaw('COUNT(*) as totalUsers')->get();
    foreach ($items as $item);
    return $item;
  }

  public function logsRole()
  {
    $items = $this->role->selectRaw('COUNT(*) as totalRoles')->get();
    foreach ($items as $item);
    return $item;
  }

  public function logsPermission()
  {
    $items = $this->permission->selectRaw('COUNT(*) as totalPermisos')->get();
    foreach ($items as $item);
    return $item;
  }

  public function topMethod()
  {
    $items = $this->log->selectRaw('route_method, COUNT(*) as totalMetodos')      
      ->groupBy('route_method')
      ->orderBy('totalMetodos', 'DESC')->take(6)->get();
    return $items;
  }

  public function topRoute()
  {
    $items = $this->log->selectRaw('route_path, COUNT(*) as totalRutas')      
      ->groupBy('route_path')
      ->orderBy('totalRutas', 'DESC')->take(6)->get();
    return $items;
  }

  public function topAlias()
  {
    $items = $this->log->selectRaw('route_alias, COUNT(*) as totalAlias')     
      ->where('route_alias',"NOT LIKE",  'generated' . "%") 
      ->Where('route_alias','!=', '') 
      ->groupBy('route_alias')
      ->orderBy('totalAlias', 'DESC')->take(6)->get();
    return $items;
  }

  public function topIp()
  {
    $items = $this->log->selectRaw('ip_address, COUNT(*) as totalIp')      
      ->groupBy('ip_address')
      ->orderBy('totalIp', 'DESC')->take(6)->get();
    return $items;
  }

  public function topAgent()
  {
    $items = $this->log->selectRaw('user_agent, COUNT(*) as totalAgent')      
      ->groupBy('user_agent')
      ->orderBy('totalAgent', 'DESC')->take(6)->get();
     
    return $items;
  }

  public function topUser()
  {
    $items = $this->log->selectRaw('user_id, COUNT(*) as totalUser')        
      ->groupBy('user_id')
      ->orderBy('totalUser', 'DESC')->take(6)->get();
     
    return $items;
  }
}
