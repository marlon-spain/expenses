<?php

declare(strict_types=1);

namespace App\Repositories\Dashboard;
use Carbon\Carbon;

use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;


class DashboardRepository extends BaseRepository
{
    public  ?object $query = null;
    protected  ?string $fromDate = null;
    protected  ?string $toDate = null;
    protected  ?string $table = null;
    protected  array $where = [];
    protected  ?string $activeFilterAuto = null;

  

    public function setTable($table){
      
        $this->table = $table;
        $this->query = DB::table($this->table);         
         return $this;
      }
    public function queryBuild(){       
      $dateField = $this->table.'.created_at';         
      if (!empty($this->where))$this->query->where($this->where);
     $query = $this->query->get();
     return $query;
   }
   public function setSelectTotalDate(){
    $dateField = $this->table.'.created_at';
      $this->query->select(DB::raw('count(*) as total,  DATE_FORMAT('.$dateField.', "%Y-%m-%e") as date'));
     return $this;
  }
  
  public function setSelectTotal(){
    $dateField = $this->table.'.created_at';
      $this->query->select(DB::raw('count(*) as total'));
     return $this;
  }

  public function setSelectTotalHour(){
    $dateField = $this->table.'.created_at';
      $this->query->select(DB::raw('count(*) as total,  DATE_FORMAT('.$dateField.', "%Y-%m-%e:%H") as date'));
     return $this;
  }

  public function setSelectPromedi(){
    $this->query->select(DB::raw('images_status.image_id, group_concat( images_status.`updated_at` ) as dates, group_concat( images_status.`status` ) as status'))    
    ;
     return $this;
  }

    public function setBetweenDate($fromDate = "2021-01-01", $toDate = '2021-12-31'){
      $this->fromDate = $fromDate;
      $this->toDate = $toDate;
        return $this;
    }
    public function setWhere($where = []){
        $this->where[] = $where;
          return $this;
      }
   
      public function buildFilterAuto($type = 'auto' ){
        $this->where[] = ['images_status.status','=', 'completed'];
        if(!isset($type))$type = 'auto';
        if($type == 'auto') $whereIn = 'whereNotIn';else $whereIn = 'whereIn';
         $this->query->$whereIn($this->table.'.image_id', function ($query) {
         $query->select('i.image_id')->from($this->table.' as i')->distinct();
         $query->where('i.status', '=', 'send');
         $query->orWhere('i.status', '=', 'working');
        });
          return $this;
      }
      
      public function buildFilterwhereIn($field = 'send' ){
         $this->query->whereIn($this->table.'.image_id', function ($query) use ($field) {
         $query->select('i.image_id')->from($this->table.' as i');
         $query->where('i.status', '=', $field);
        });
          return $this;
      }
    
      public function setReturnTotal($query){      
        $out = ['total'=> 0, 'perDate' => $query];       
        foreach ($query as $key => $value){           
          $out['total'] = $out['total']+ $value->total; 
        }    
         return $out;
      } 

     
      public function setReturnTotalRatioTest($query){      
        $out = ['total'=> 0, 'perDate' => $query];           
        foreach ($query as $key => $value){   
        //  $test = (new MdmConnectionService())->findTest($value->test);        
        //  $value->test = $test;           
          $out['total'] = $out['total']+ $value->Importe; 
        }    
         return $out;
      }       

      public function setDefaultReturnTotal($query){
        $out = ['total'=> count($query), 'data' => $query, 'where' => $this->where];
        $out['query'] = DB::getQueryLog();
        return $out;
      }

      public function setSendCompleted($response, $statusFrom = 'send', $statusTo = 'completed'){
        foreach ($response as $value) {
          $statusArray = explode(',', $value->status);
          $xStatusSend = null;
          foreach ($statusArray as $key => $status) {
            if( $status ==  $statusFrom) $xStatusSend = $key;
          }
          
          $xStatusCompleted = null;
          foreach ($statusArray as $key => $status) {
            if( $status == $statusTo) $xStatusCompleted = $key;
          }
    
          $dateArray = explode(',', $value->dates);         
          $value->dateIn = @$dateArray[$xStatusSend];
          $value->dateOut = @$dateArray[$xStatusCompleted];
          $value->diffSec = (new Carbon($value->dateIn))->diffInSeconds(new Carbon($value->dateOut));
        }
        return $response;
      }
      public function getPromedi($response, $statusFrom = 'send'){
        $total = 0;
        $countResponse = 0;
        foreach ($response as $value) {
          if(isset($value->dateIn) AND isset($value->dateOut)){
            $total = $total + $value->diffSec; 
            $countResponse = $countResponse + 1; 
          }  
          else{
            $total = $total + 0; 
          }
        }
        if($total !=0) $seconds = $total / count($response); else $seconds = 0;
        $out = ['timeAv' => gmdate("H:i:s", $seconds),'totalCountAv'=>$countResponse, 'totalCount'=>count($response), 'data'=> $response];
        $out['query'] = DB::getQueryLog();
        return  $out;
      }    
}
