<?php

declare(strict_types=1);

namespace App\Repositories\Dashboard;

use App\Models\ActivityLog;
use Carbon\Carbon;
use App\Models\Recibo;
use App\Models\Tipo_gasto;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;


class KpisRepository extends BaseRepository
{
  public  ?object $query = null;
  protected  ?string $fromDate = null;
  protected  ?string $toDate = null;
  protected  ?string $table = null;
  protected  array $where = [];
  protected  ?string $activeFilterAuto = null;

  public function __construct(
    private Recibo $recibo,
    private ActivityLog $log,
    private Tipo_gasto $tipoGasto
  ) {
  }

  public function recibos()
  {   
    $items =$this->recibo->selectRaw('COUNT(id) as totalRecibos, SUM(importe) as Importe')->get();           
    $puntos = [];
    foreach ($items as $i) {
      $i->estado;
      $i->proceso;     

      $puntos[] = [
        'x' => $i['totalRecibos'],
        'y' => floatval($i['Importe']),
      ];
    }
    return $puntos;
  }

  public function topRecibos(){
    $items = $this->recibo->selectRaw('estado_id, COUNT(*) as totalRecibos, SUM(importe) as Importe')                 
    ->groupBy('estado_id')
    ->orderBy('totalRecibos','DESC')->take(5)->get(); 

    $puntos = [];
    foreach ($items as $i) {
      $i->estado_recibo->nombre;
      $i->proceso;          
      $puntos[] = [
        'x' => $i['totalRecibos'],
        'y' => floatval($i['Importe']),
        'z' => $i['estado_recibo']['nombre'],
      ];
    }

    return $puntos;
  }
  public function topGastos(){ //por año y mes agrupado
    $items = $this->recibo->selectRaw('year(fecha) year, monthname(fecha) month ,COUNT(*) as totalRecibos, SUM(importe) as Importe')                 
    ->groupBy('year','month')
    ->orderBy('totalRecibos','DESC')->take(12)
    ->get(); 

    $puntos = [];
    foreach ($items as $i) {
           
      $puntos[] = [
        'x' => $i['totalRecibos'],
        'y' => floatval($i['Importe']),
        'z' => $i['year'],
        'a' => $i['month'],
      ];
    }  
    return $puntos;
  }

  public function logs(){ //por año y mes agrupado
    $items = ActivityLog::select(DB::raw("COUNT(*) as count"))                 
                        ->whereYear("created_at", date('Y'))
                        ->groupBy(DB::raw("Month(created_at)"))
                        ->pluck('count');                    
    return $items;
  }

  
  public function getInfluxByUser($user)
  {
    DB::statement("SET lc_time_names = 'es_ES';"); // formato de día en español
    $daysOfWeek = ['lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado', 'domingo'];
    $dates = [
      $startDate = Carbon::now()->subDays(89)->format('Y-m-d H:i:s'),
      $endDate = Carbon::now()->format('Y-m-d H:i:s')
    ];
    $influx = [];
    
    // Agrupar los resultados por día de la semana y contar los registros
    $results = Recibo::selectRaw('DATE_FORMAT(fecha, "%w") as idDay, DATE_FORMAT(fecha, "%W") as day, count(*) as total')
      ->whereBetween('fecha', $dates)
      ->where('user_id', $user)
      ->groupBy('idDay', 'day')
      ->get();
    
    // Crear una matriz con los resultados para cada día de la semana
    foreach ($daysOfWeek as $i => $dayOfWeek) {
      $entry = $results->firstWhere('idDay', (string)$i);
      $influx[] = [
        'data' => $i,
        'day' => $dayOfWeek,
        'total' => $entry ? $entry->total : 0,
      ];
    }
    return $influx;
  }

  public function getRequestWeekTypeParam($param, $column)
  {
    DB::statement("SET lc_time_names = 'es_ES';"); // formato de día en español
    $endDate = Carbon::now()->endOfDay(); // Fecha actual
    $startDate = $endDate->copy()->subDays(6)->startOfDay(); // Fecha hace 7 días
  
    $weekDays = ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'];
  
    // Obtener todos los días dentro del rango
    $allDays = [];
    $date = $startDate->copy();
    while ($date <= $endDate) {
      $allDays[] = $date->format('Y-m-d');
      $date->addDay();
    }
  
    $entries = Tipo_gasto::selectRaw('tipo_gastos.nombre, DATE(recibos.fecha) as date, COUNT(recibos.id) as total')
      ->leftJoin('recibos', 'tipo_gastos.id', '=', 'recibos.tipo_gasto_id')
      ->where("recibos.$column", $param)
      ->whereDate('recibos.fecha', '>=', $startDate->format('Y-m-d'))
      ->whereDate('recibos.fecha', '<=', $endDate->format('Y-m-d'))
      ->groupBy('date', 'tipo_gastos.nombre') // Modificar el orden de agrupamiento
      ->orderBy('date')
      ->get();
  
    $influx = [];
    $date = $startDate->copy();
    foreach ($allDays as $day) {
      $dayOfWeek = $weekDays[$date->dayOfWeek];
      $entriesByDate = $entries->where('date', $day);
  
      // Usar reduce para construir un array asociativo de tipos de pago
      $lisFormsData = $entriesByDate->reduce(function ($carry, $entry) {
        $carry[] = [
          'nombre' => $entry->nombre,
          'total' => intval($entry->total),
        ];
        return $carry;
      }, []);
  
      $influx[] = [
        'data' => $day,
        'day' => $dayOfWeek,
        'tipo_pago' => $lisFormsData,
      ];
  
      $date->addDay();
    }
  
    return $influx;
  }

}
