<?php

namespace App\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

class BaseRepository
{
    protected function model(): ?string
    {
        return null;
    }

    protected function entity(): ?string
    {
        return null;
    }

    protected function query(): Builder
    {
        return app($this->model())->newQuery();
    }

    public function all()
    {
        return $this->query()->get();
    }

    public function searchPaginated(array $filters = []): LengthAwarePaginator
    {
        $query = $this->query();

        foreach ($filters['fields'] ?? [] as $field) {
            $query->orWhere($field, 'like', '%' . ($filters['search'] ?? '') . '%');
        }

        return $query->orderBy('id', 'desc')->paginate($filters['per_page'] ?? 20);
    }

    public function allPaginated(?int $perPage = 20): LengthAwarePaginator
    {
        return $this->searchPaginated(['per_page' => $perPage]);
    }

    public function where($column, $operator = null, $value = null)
    {
        return $this->query()->where($column, $operator, $value);
    }

    public function find(int $id)
    {
        return $this->query()->find($id);
    }

    public function findOrFail(int $id)
    {
        return $this->query()->findOrFail($id);
    }

    public function create(array $attributes)
    {
        return $this->query()->create($attributes);
    }

    public function update(int $id, array $attributes)
    {
        $this->find($id)->update($attributes);
        return $this->find($id);
    }

    public function delete(int $id)
    {
        return $this->findOrFail($id)->delete();
    }
}
