<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Recibo;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReciboPolicy
{
    use HandlesAuthorization;
    
    public function author(User $user, Recibo $recibo)
    {
        
       if($user->id == $recibo->id){
           return true;
       }
       else{
           return false;
       }   
    }

}
