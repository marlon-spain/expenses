<?php

namespace App\Policies;

use App\Models\Nomina;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NominaPolicy
{
    use HandlesAuthorization;

    
    public function viewAny(User $user)
    {
        //
    }

  
    public function view(User $user, Nomina $nomina)
    {
        //
    }

  
    public function create(User $user)
    {
        //
    }

    public function update(User $user, Nomina $nomina)
    {
        //
    }

    
    public function delete(User $user, Nomina $nomina)
    {
        
        return $user->id === $nomina->user_id;

    }

  
    public function restore(User $user, Nomina $nomina)
    {
        //
    }

  
    public function forceDelete(User $user, Nomina $nomina)
    {
        //
    }
}
