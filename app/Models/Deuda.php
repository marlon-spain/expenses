<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deuda extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'fecha_inicio',
        'nombre',
        'fecha_fin',
        'total',
        'restante',
        'estado_deuda_id',
        'Proceso_deuda_id',
        'deuda_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $with = ['user'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function estado()
    {
        return $this->belongsTo(Estado_deuda::class, 'estado_deuda_id');
    }

    public function proceso()
    {
        return $this->belongsTo(Proceso_deuda::class, 'Proceso_deuda_id');
    }

    public function recibo()
    {
        return $this->hasMany(Recibo::class, 'id');
    }
}
