<?php

namespace App\Models;

use App\Models\MainModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TimeUserSession extends MainModel
{
    use HasFactory;


    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'date',     
        'active',  
        'login',
        'logout',
        'time'    
    ];
    
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $with=['user'];   

    public function user()
     {
         return $this->belongsTo(User::class, 'user_id');
     }
 
}
