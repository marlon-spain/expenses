<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estado_recibo extends Model
{
    use HasFactory;
    protected $fillable=[
        'nombre',
      
    ];

    public function recibo(){       
        return $this->hasMany(Recibo::class, 'estado_id');
   }
}
