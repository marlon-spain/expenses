<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proceso_deuda extends Model
{
    use HasFactory;
    protected $fillable =[
        'nombre'
    ];

    
    public function estado(){       
        return $this->hasMany(Deuda::class, 'estado_deuda_id');
   }

}
