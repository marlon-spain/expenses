<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use synlab\synlab_ratings\Models\MainModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Recibo extends MainModel
{
    use HasFactory;

    protected $table = 'recibos';
    protected $primaryKey = 'id';
    protected $allowedKeys = ['id'];
    protected $orderBy = ['id', 'desc'];
    static public $requestFilters = ['descripcion','fecha','importe'];

    protected $fillable = [
        'user_id',
        'fecha',
        'descripcion',
        'importe',
        'cuenta_id',
        'tipo_gasto_id',
        'gasto_fijo_id',
        'estado_id',
        'tipo_pago_id',
        'nomina_id',
        'deuda_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'      
    ];

    protected $with=['user'];   

    public function user()
     {
         return $this->belongsTo(User::class, 'user_id');
     } 
    
    public function cuenta()
    {
        return $this->belongsTo(Cuenta::class, 'cuenta_id');
    }
    public function tipo_gasto()
    {
        return $this->belongsTo(Tipo_gasto::class, 'tipo_gasto_id');
    }
    public function gasto_fijo()
    {
        return $this->belongsTo(Gasto_fijo::class, 'gasto_fijo_id');
    }
    public function estado_recibo()
    {
        return $this->belongsTo(Estado_recibo::class, 'estado_id');
    }
    public function tipo_pago()
    {
        return $this->belongsTo(Tipo_pago::class, 'tipo_pago_id');
    }
    public function nomina()
    {
        return $this->belongsTo(Nomina::class, 'nomina_id');
    }
    public function deuda()
    {
        return $this->belongsTo(Deuda::class, 'deuda_id');
    }   

    public function seguimiento()
    {
        return $this->hasMany(Seguimiento_recibo::class, 'recibo_id')->orderBy('id', 'desc');
    }
}
