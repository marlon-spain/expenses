<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seguimiento_recibo extends Model
{
    use HasFactory;
    
    protected $primaryKey = 'id';
    protected $fillable =[
    'user_id',
    'recibo_id',
    'estado_recibo_id',
    'comentarios',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function recibo()
    {       
        return $this->hasMany(Recibo::class, 'id');
    }

    public function estado()
    {
        return $this->belongsTo(Estado_recibo::class, 'estado_recibo_id');
    }

}
