<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use function PHPUnit\Framework\returnSelf;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use betterapp\LaravelDbEncrypter\Traits\EncryptableDbAttribute;

class ActivityLog extends Model
{
  
    protected $table = 'activity_logs';
    protected $encryptable = [
        'request_body'
    ];
    protected $with=['user'];

    protected $fillable = [
        'id',
        'user_id',
        'user_permissions',
        'route_path',
        'route_method',
        'route_alias',
        'request_body',
        'request_headers',
        'user_agent',
        'ip_address',
        'is_proxy',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    static function storeActivity($activity)
    {
        $result = self::create($activity);
        return $result;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
