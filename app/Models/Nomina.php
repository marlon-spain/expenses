<?php

namespace App\Models;

use App\Models\MainModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Nomina extends MainModel
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'fecha',
        'devengo',
        'deducciones',
        'IRPF',
        'remuneracion',
        'restante',
        'comentarios',
        'empresa_id'

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
     'created_at',
     'updated_at'
    ];
    
    protected $with=['user'];   

    public function user()
     {
         return $this->belongsTo(User::class, 'user_id');
     }
 

    public function recibo(){       
        return $this->hasMany(Recibo::class, 'tipo_pago_id');
   }

   public function empresa()
   {
       return $this->belongsTo(Empresa::class, 'empresa_id');
   }

 
}
