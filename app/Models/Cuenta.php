<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuenta extends MainModel
{
    use HasFactory;
    protected $fillable=[
        'user_id',
        'nombre',
        'IBAN',
        'tipo',
        'active'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $with=['user'];

   

   public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    



}
