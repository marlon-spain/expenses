<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Laravel\Jetstream\HasProfilePhoto;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Cuenta;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;


    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    protected $appends = [
        'profile_photo_url',
    ];

    protected $with = [];


    public function cuentas()
    {
        return $this->hasMany(Cuenta::class, 'user_id');
    }

    public function deudas()
    {
        return $this->hasMany(Deuda::class, 'user_id');
    }

    public function nominas()
    {
        return $this->hasMany(Nomina::class, 'user_id');
    }

    
    public function recibos()
    {
        return $this->hasMany(Recibo::class, 'user_id');
    }

    public function logs()
    {
        return $this->hasMany(ActivityLog::class, 'user_id');
    }

    public function timeUserSession()
    {
        return $this->hasMany(TimeUserSession::class, 'user_id');        
    }
}
