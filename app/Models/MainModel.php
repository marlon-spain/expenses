<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class MainModel extends Model
{
    static protected $requestFilters = ['description'];
    protected $allowedKeys = ['id'];
    protected $fillableUpdate = [];
    protected $storeUnique = false;
    protected $allowsUpdates = true;
    protected $hidden = ['deleted_at'];
    protected $orderBy = ['id','desc'];
    public $orderByRawStr = null;
    use HasFactory;
    static function getData(Request $request){
            $where = [];
            if ($request->filled($request->per_page))$request->request->add(['per_page' => 20]);
            if ($request->hasAny(static::$requestFilters)) {
        foreach (static::$requestFilters as $key => $value) {
            if($request->has($value)):
                $where[] = [$value, 'like','%' . $request->$value . '%'];
            endif;
                    }
            }
            if (!empty($where)):
                $query = self::where($where)
                           ->orderBy('id', 'desc')
                           ->paginate($request->per_page);
                     return $query;
            endif;
            $query = self::orderBy('id', 'desc')->paginate($request->per_page);
            return $query;
    }
    public function storeData(Request $request){

        $validator = Validator::make($request->all(),$this->rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
          $this->errorExit($errors);
        }
        $tosave = array();
        foreach ($this->fillable as $key => $value) {
            if($request->has($value)) $tosave[$value] = $request->$value;
        }
        try {
            if (!$this->storeUnique)$meth = 'create'; else $meth = 'updateOrCreate';
            $save = $this->$meth($tosave);
            return $save->fresh();
        } catch (\Illuminate\Database\QueryException $exception) {
            return $exception->errorInfo;
        }
    }
    public function updateData(Request $request, $id = null, $keyType = 'id'){

        if (empty($id)) return ['empty id to update'];
        if (!in_array($keyType, $this->allowedKeys)) return ['keyType not allowed'];
        $validator = Validator::make($request->all(),$this->rulesUpdate());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $this->errorExit($errors);
          return $errors;
        }

        try {
            $update = $this->where($keyType, $id)->first();
            if (!$this->allowsUpdates)$this->errorExit("this model does not allow updates");
            foreach ($this->fillableUpdate as $key => $value) {
                if($request->has($value)) $update->$value = $request->$value;
             }
             $update->save();
             return $update;
        } catch (\Illuminate\Database\QueryException $exception) {
            return $exception->errorInfo;
        }
    }
    public function deleteItem($id = null, $keyType = 'id'){
        if (empty($id)) return ['empty id to update'];
        if (!in_array($keyType, $this->allowedKeys)) return ['keyType not allowed'];

        try {
            return $this->where($keyType, $id)->delete();
        } catch (\Illuminate\Database\QueryException $exception) {
            return $exception->errorInfo;
        }
    }


    static function search(Request $request){
      //  DB::connection()->enableQueryLog();
      if ($request->filled($request->per_page))$request->request->add(['per_page' => 20]);
        $items = self::select();
        if($request->has('fields')) {
            foreach($request->fields as $field){
                if (in_array($field, static::$requestFilters)){
         $items = $items->orWhere($field, 'like', '%'.$request->search.'%');
                }
            }
        }
        $response = $items->orderBy('id', 'desc')->paginate($request->per_page);
        //$queries = DB::getQueryLog();
    // $response['queries'] = $queries;
        return $response;
    }
    public function searchWhere($request, $whereFilterSearch = [], $joinWhere = [], $selectFields = []){
        //  DB::connection()->enableQueryLog();
        if ($request->filled($request->per_page))$request->request->add(['per_page' => 20]);
         $this->requestT = $request;
         $response = $this->where($whereFilterSearch)->where( function($query) {
                      if($this->requestT->has('fields')) {
                          foreach($this->requestT->fields as $field){
                              if (in_array($field, static::$requestFilters)){
                                  $meth = 'orWhere';
                                  if (!empty($this->encryptable) AND (in_array($field, $this->encryptable)))$meth = 'orWhereEncrypted';
                                  $query->$meth($this->getTable().'.'.$field, 'like', '%'.$this->requestT->search.'%');
                              }
                          }
                      }
          });
          if(!empty($joinWhere)){
              foreach ($joinWhere as $jw) {
                $response->join($jw['table'], function ($join) use ($jw) {
                    $join->on($jw['table'].'.id', '=', $this->getTable().'.'.$jw['table'].'_id')
                         ->where($jw['table'].'.'.$jw['field'], '=', $jw['value']);
                });
               if(!empty($selectFields))$selectFields = array_merge($selectFields, [$jw['table'].'.'.$jw['field']]);
              }
          }
          if(!empty($selectFields)){
            $response->select($selectFields);
          }
          if(isset($this->orderByRawStr)) $response->orderByRaw($this->orderByRawStr); else $response->orderBy($this->getTable().'.'.$this->orderBy[0], $this->orderBy[1]);
          $response = $response->paginate($request->per_page);
          $this->requestT = null;
          $this->orderByRawStr = null;
       //   $queries = DB::getQueryLog(); print_r($queries);exit;
          return $response;
      }

    public static function errorExit ($message, $statusCode = 421){
        $returnData = array(
            'status' => 'error',
            'message' => $message
        );
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Allow: GET, POST, OPTIONS, PUT, DELETE");

        header('Content-Type: application/json');
        http_response_code($statusCode);
         print_r(json_encode($returnData));exit;
    }

    static function hasManyByCode(
        $tableToGet,
        $tableToJoin,
        $Fild,
        $FildJ,
        $codeWhere
        )
    {
        $key = 'code';
        $lab = DB::table($tableToGet)
         ->join($tableToJoin, $tableToGet.'.'.$key, '=', $tableToJoin.'.'.$Fild)
         ->where($tableToJoin.'.'.$FildJ, '=', $codeWhere)
         ->select($tableToGet.'.id',$tableToGet.'.'.$key,$tableToGet.'.description' )
         ->get();
       return $lab;
    }
    static function hasManyById($table,$tableJ,$idWhere,$returnable = '*')
    {
        $tableToJoin = $table.'_'.$tableJ;
        $lab = DB::table($table)
         ->join($tableToJoin, $table.'.id', '=', $tableToJoin.'.'.$table.'_id')
         ->where($tableToJoin.'.'.$tableJ.'_id', '=', $idWhere)
         ->select($returnable)
         ->get();
       return $lab;
    }
    public function storeChild($store,$idKey, $idValue = null){
        try {
            $store[$idKey] = $idValue;
            return $this->create($store);
        } catch (\Illuminate\Database\QueryException $exception) {
            return $exception->errorInfo;
        }

    }
    public function getChild ($lotId, $table){
        if(!empty($this->parentReturnable)){
            $response = $this->hasManyById($this->parentTable,$table,$lotId,$this->returnables());
            if($response->isEmpty()){
                $response = self::where($table.'_id', $lotId)
            ->get($this->returnable);
          }
        }
        else{
            $response = self::where($table.'_id', $lotId)
            ->get($this->returnable);
        }
        return $response;
    }

    public function storeRelated ($store, $tosave){
    }

    public function upsertDb($toSave, $model, $limit = 1000, $fields = ['code', 'description'], $unique = ['code'])
    {
       $array_chunk = array_chunk($toSave, $limit);
       $toInsert = [];
       foreach($array_chunk as $a_chunk){
        $array = [];
        foreach($a_chunk as $s){
            foreach($fields as $key)if(isset($s[$key]))$array[$key] = $s[$key];
            $toInsert[] = $array;
        }
        $model::upsert($toInsert,$unique);
       }
    }


}
