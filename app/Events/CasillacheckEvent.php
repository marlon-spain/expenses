<?php

namespace App\Events;

use Carbon\Carbon;
use App\Models\Cuenta;
use Illuminate\Broadcasting\Channel;
use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class CasillacheckEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $id;
    public $active;

    public function __construct($id, $active)
    {
        $this->id = $id;
        $this->active = $active;
        info("fff");
    }

    public function broadcastOn()
    {
        return new channel('casilla-channel.' . $this->user->id);
    }

    public function broadcastAs()
    {
        return 'CasillaEvent';
    }

    public function broadcastWith()
    {
        return [
            'id' => $this->id,
            'active' => $this->active
        ];
    }

    public function broadcast()
    {
        $cuenta = Cuenta::find($this->id);

        if ($cuenta) {
            $cuenta->update(['active' => $this->active]);
        }

        return [
            'id' => $this->id,
            'active' => $this->active
        ];
    }
}
