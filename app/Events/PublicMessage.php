<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class PublicMessage implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;  
 
    public function __construct()
    {
          
    }
    
    public function broadcastOn()
    {
        return new Channel('public-message-channel');
        
    }
   
    public function broadcastAs()
    {
        return 'MessageEvent';
    }

    public function broadcastWith()
    {
        return [
            'type' => 'api-message-type-2',
            'appPuerto' => 'http://expenses.local:6001'."2",
            'details' => "Channel:public-message-webSocket- App\Events\SendMessage",
            'time' => Carbon::now()->format('H:i:s')
        ];
    }
}
