<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class GraficoEvent implements ShouldBroadcastNow, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, SerializesModels;


    public $items;
    public $puntos;
    public $puntos2;

    public function __construct($items, $puntos, $puntos2)
    {
        $this->items = $items;
        $this->puntos = $puntos;
        $this->puntos2 = $puntos2;
     //   dd($this->items,  $this->puntos,  $this->puntos2);
    }


    public function broadcastOn()
    {
        $connection = 'database'; // Reemplaza esto con el nombre de tu conexión
        $queue = 'default'; // Reemplaza esto con el nombre de tu cola        
        Queue::connection($connection)->pushOn($queue, $this); //$this instancia la misma clase del evento
        return new Channel('grafico-channel');
    }

    public function broadcastAs()
    {
        return 'GraficoEvent';
    }

    public function broadcastWith()
    {  
        return [
            'items' =>  $this->items,
            'puntos' => $this->puntos ,
            'puntos2' =>  $this->puntos2,"ggggg" ,
            'time' => "cambio"
        ];
    }
}


