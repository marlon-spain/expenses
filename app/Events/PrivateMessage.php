<?php

namespace App\Events;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class PrivateMessage implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

      // public $user;

    public function __construct(User $user)
    {  
        $this->user = $user;
    }


    public function broadcastOn()
    {
        return new PrivateChannel('message-channel.' . $this->user->id);
    }

    public function broadcastAs()
    {
        return 'MessageEvent';
    }

    public function broadcastWith()
    {
        return [
            'type' => 'api-private',
            'appPuerto' => 'http://expenses.local:6001' . "priv",
            'details' => "Channel:private-message-webSocket- App\Events\SendMessage",
            'time' => Carbon::now()->format('H:i:s')
        ];
    }
}
