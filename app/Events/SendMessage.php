<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class SendMessage implements ShouldBroadcastNow, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, SerializesModels;


    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function broadcastOn()
    {
        $connection = 'database'; // Reemplaza esto con el nombre de tu conexión
        $queue = 'default'; // Reemplaza esto con el nombre de tu cola        
        Queue::connection($connection)->pushOn($queue, $this); //$this instancia la misma clase del evento
        return new Channel('user-channel');
    }

    public function broadcastAs()
    {
        return 'UserEvent';
    }

    public function broadcastWith()
    {
        return [
            'type' => 'api-message-type',
            'appPuerto' => 'http://expenses.local:6001',
            'details' => "Channel:public-webSocket- App\Events\SendMessage",
            'time' => Carbon::now()->format('H:i:s')
        ];
    }
}



