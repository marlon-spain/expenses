<?php

namespace App\Listeners;

use DateTime;
use App\Models\TimeUserSession as model;
use App\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
 

class SuccessfulLogin
{
    public function __construct(
        private model $sesion,
     
    )
    {
    }
   

    /**
     * Handle the event.
     *
     * @param  \Illuminate\Auth\Events\Login  $event
     * @return void
     */
    public function handle(Login $event)
    {        
       //dd(Auth::user()->id);          
        $activity = [
            "user_id" => $event->user['id'],    
            "date" => new DateTime,
            "active" => 1,           
            "login" => $event = new DateTime         
        ];
        $register =model::create($activity);
        $register->save();    
     
    }
}
