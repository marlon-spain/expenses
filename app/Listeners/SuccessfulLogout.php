<?php

namespace App\Listeners;

use DateTime;
use App\Models\TimeUserSession as model;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Logout;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuccessfulLogout
{

    public function __construct(
        private model $sesion,

    ) {
    }


    public function handle(Logout $event)
    {
        $hora = now()->isoFormat('H:mm:ss');
        $id = model::where('user_id', '=', Auth::user()->id)
                 ->where('active', '=', 1)->get();
        foreach ($id as $item) {
            $hi = $item->login;
            $total = date("H:i:s", strtotime($hora) - strtotime($hi));
            $item->time = $total;
            $item->update([
                'active' => 0,
                'date' => new DateTime,
                'logout' => new DateTime,
                'time' =>  $total
            ]);
        }
    }
}
