@extends('adminlte::page')
@section('title', 'cuentas')

@section('content')
<section class="section">
    <div class="section-header">
        <h3 class="page_heading">Editar cuentas</h3>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">


                        @if ($errors->any())
                        <div class="alert alert-dark alert-dismissible fade show" cuenta="alert">
                            <strong>Revise los campos!</strong>
                            @foreach($errors->all() as $error)
                            <span class="badge badge-danger">{{$error}}</span>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        {!! Form::model($edit, ['method'=>'PATCH','route'=>['cuenta.update',$edit->id]]) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">

                                <div class="form-group">
                                    <label for="name">Nombre </label>
                                    {!! Form::text('nombre', null, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    <label for="name">IBAN</label>
                                    {!! Form::text('IBAN', null, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    <label for="name">tipo</label>
                                    {!! Form::text('tipo', null, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    <label for="name">Activo</label>
                                    <br>
                                    {!! Form::hidden('active', 0) !!}
                                    {!! Form::checkbox('active', 1, $edit->active, ['class' => 'name']) !!}
                                </div>

                                <button type="submit" class="btn btn-primary">Guardar</button>

                            </div>
                        </div>

                        {!! Form::close() !!}



                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


@stop