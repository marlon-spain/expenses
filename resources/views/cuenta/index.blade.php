@extends('adminlte::page')

@section('title', 'items')


@section('content')
<div class="col-lg-12 mt-2">
    <div class="card w-100 h-100">
        <div class="card-body">
            <h7 class="text-right">Registro de cuentas: {{ auth()->user()->name }}</h7>

            <br>
            <div class="text-right">
                <a href="{{route('cuenta.create')}}" class="btn btn-primary mb-3">Crear Registro</a>
            </div>

            <div class="table-responsive">
                <table id="items" class="table table-hover table-striped table-bordered table-sm" style="width:100%;">
                    <thead>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">user</th>
                            <th scope="col">nombre</th>
                            <th scope="col">IBAN</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">activo</th>
                            <th scope="col">acciones</th>

                        </tr>

                    </thead>

                </table>
            </div>

        </div>
    </div>
</div>
@stop

@section('js')
@include('layouts.datatable', [
'ajaxUrl' => route('cuenta.index'),
'columns' => json_encode([
['data' => 'id'],
['data' => 'user.name'],
['data' => 'nombre'],
['data' => 'IBAN'],
['data' => 'tipo'],
['data' => 'active'],
['data' => 'action', 'orderable' => false],
])
])

@include('layouts.delete', ['deleteUrl' => "nomina"])





<script>
    function updateActiveStatus(checkbox, id) {
        var activeValue = checkbox.checked ? '1' : '0';
        fetch('/cuenta/change/' + id, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                },
                body: '_method=PUT&active=' + activeValue
            })
            .then(response => {
                if (response.ok) {
                    console.log('Estado activo actualizado correctamente');
                } else {
                    console.error('Error al actualizar el estado activo');
                }
            })
            .catch(error => {
                console.error('Error de red:', error);
            });
    }
</script>

@stop