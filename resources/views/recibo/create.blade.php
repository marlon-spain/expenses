@extends('adminlte::page')
@section('title', 'deudas')

@section('content')

                <div class="card">
                    <div class="card-body">


                        @if ($errors->any())
                        <div class="alert alert-dark alert-dismissible fade show" deuda="alert">
                            <strong>Revise los campos!</strong>
                            @foreach($errors->all() as $error)
                            <span class="badge badge-danger">{{$error}}</span>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        {!! Form::open(array('route'=>'recibo.store', 'method'=>'POST')) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                    <label for="name">Fecha</label>
                                    {!! Form::date('fecha', null, array('class'=>'form-control')) !!}
                                </div>

                            <div class="form-group">
                                    <label for="name">descripcion</label>
                                    {!! Form::text('descripcion', null, array('class'=>'form-control', 'placeholder'=>'si es ingreso la nomina actual sumara, si no se restara') ) !!}
                                </div>

                                <div class="form-group">
                                    <label for="name">Importe</label>
                                    {!! Form::number('importe',null,['class' => 'form-control','step'=>'any', 'placeholder'=>' importe de ejemplo 300.00€']) !!}
                                </div>

                                <div class="form-group">
                            {!! Form::label('cuenta_id', 'Cuenta', ['class' =>'form-label']) !!}
                            <select name="cuenta_id" class="form-control" >
                                @foreach($cuenta as $c)
                                 <option value="{{$c->id}}" >-- {{$c->nombre}} -- Numero de cuenta:{{$c->IBAN}} --</option>                        
                                @endforeach
                            </select>
                                </div>

                                <div class="form-group">
                            {!! Form::label('tipo_gasto_id', 'Gasto', ['class' =>'form-label']) !!}
                            <select name="tipo_gasto_id" class="form-control" >
                                @foreach($tipo_gasto as $t)
                                 <option value="{{$t->id}}" >{{$t->nombre}}</option>                        
                                @endforeach
                            </select>
                                </div>

                                <div class="form-group">
                            {!! Form::label('gasto_fijo_id', 'Tipo Gasto', ['class' =>'form-label']) !!}
                            <select name="gasto_fijo_id" class="form-control" >
                            <option selected disabled>-- Please select one option --</option>
                                @foreach($gasto_fijo as $g)
                                 <option value="{{$g->id}}" >{{$g->nombre}}</option>                        
                                @endforeach
                            </select>
                                </div>
                                                                
                                <div class="form-group">
                            {!! Form::label('deuda_id', 'Deuda', ['class' =>'form-label']) !!}
                            <select name="deuda_id" class="form-control" >
                            <option selected disabled>-- Please select one option --</option>
                                @foreach($deuda as $d)                                         
                                 <option value="{{$d->id}}" >{{$d->nombre}}</option>                        
                                @endforeach
                            </select>
                                </div>     
                               

                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}



                    </div>
                </div>
            </div>
    


@stop


