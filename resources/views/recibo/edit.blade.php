@extends('adminlte::page')
@section('title', 'deudas')

@section('content')
<section class="section">
    <div class="section-header">
        <h3 class="page_heading">Actualizar campos</h3>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">


                        @if ($errors->any())
                        <div class="alert alert-dark alert-dismissible fade show" deuda="alert">
                            <strong>Revise los campos!</strong>
                            @foreach($errors->all() as $error)
                            <span class="badge badge-danger">{{$error}}</span>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        {!! Form::model($edit, ['route' => ['recibo.update', $edit->id], 'method' => 'PUT']) !!}

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                    <label for="name">Fecha</label>
                                    {!! Form::date('fecha', null, array('class'=>'form-control')) !!}
                                </div>
                                
                            <div class="form-group">
                                    <label for="name">descripcion</label>
                                    {!! Form::text('descripcion', null, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    <label for="name">Importe</label>
                                    {!! Form::number('importe', null, array('class'=>'form-control', 'placeholder'=>'Importe €')) !!}
                                </div>

                                <div class="form-group">                            
                                {!! Form::label('cuenta_id', 'Cuenta', ['class' =>'form-label']) !!}
                            <select name="cuenta_id" class="form-control" >                             
                                @foreach($cuenta as $c)
                                <option value="{{$c->id}}" {{@$c->id == $edit->cuenta_id ? 'selected' : '' }} >{{@$c->nombre}}</option>                      
                                @endforeach
                            </select>
                                </div>

                                <div class="form-group">
                            {!! Form::label('tipo_gasto_id', 'Gasto', ['class' =>'form-label']) !!}
                            <select name="tipo_gasto_id" class="form-control" >
                                @foreach($tipo_gasto as $t)
                                 <option value="{{$t->id}}" {{@$t->id == $edit->tipo_gasto_id ? 'selected' : '' }} >{{@$t->nombre}}</option>                        
                                @endforeach
                            </select>
                                </div>

                                <div class="form-group">
                            {!! Form::label('gasto_fijo_id', 'Tipo Gasto', ['class' =>'form-label']) !!}
                            <select name="gasto_fijo_id" class="form-control" >
                                @foreach($gasto_fijo as $g)
                                 <option value="{{$g->id}}" {{@$g->id == $edit->gasto_fijo_id ? 'selected' : '' }} >{{@$g->nombre}}</option>                         
                                @endforeach
                            </select>
                                </div>                                

                                <div class="form-group">
                            {!! Form::label('estado_id', 'Estado', ['class' =>'form-label']) !!}
                            <select name="estado_id" class="form-control" >
                                @foreach($estado as $e)
                                 <option value="{{@$e->id}}" {{@$e->id == $edit->estado_id ? 'selected' : '' }} >{{@$e->nombre}}</option>                         

                                @endforeach
                            </select>
                                </div>

                                <div class="form-group">
                            {!! Form::label('tipo_pago_id', 'Tipo Pago', ['class' =>'form-label']) !!}
                            <select name="tipo_pago_id" class="form-control" >
                                @foreach($tipo_pago as $p)
                                 <option value="{{@$p->id}}" {{@$p->id == $edit->tipo_pago_id ? 'selected' : '' }} >{{@$p->nombre}}</option>                         

                                @endforeach
                            </select>
                                </div>

                                <div class="form-group">
                            {!! Form::label('nomina_id', 'Nomina', ['class' =>'form-label']) !!}
                            <select name="nomina_id" class="form-control" >
                                @foreach($nomina as $n)
                                 <option value="{{@$n->id}}" {{@$n->id == $edit->nomina_id ? 'selected' : '' }} >-- {{$n->fecha}} -- Importe: {{$n->remuneracion}}</option>                         

                                @endforeach
                            </select>
                                </div>
                              
                                
                                <div class="form-group">                            
                                <label for="name">Deuda:
                                @if( empty(@$edit->deuda->nombre))
                                no existe deuda asociada
                                @else
                                {{ @$edit->deuda->nombre}} 
                                @endif                          
                            </label>                                                          
                                </div>                                    

                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}



                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


@stop