@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
<h4>Recibos detallados con estadisticas</h4>
@stop

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="table-responsive">
        <h7>Tabla info Recibo Actual</h7>
        <table class="table table-bordered table-vertical">
          <tbody>
            <!-- Contenido de la primera tabla -->
            <tr>
              <th scope="row" class="text-left">ID:</th>
              <td class="text-left">{{$item->id}}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Descripcion:</th>
              <td class="text-left">{{$item->descripcion}}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">importe:</th>
              <td class="text-left">{{$item->importe}}€</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Cuenta:</th>
              <td class="text-left">{{$item->cuenta->nombre}}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Tipo:</th>
              <td class="text-left">{{$item->tipo_gasto->nombre}}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Gasto:</th>
              <td class="text-left">{{$item->gasto_fijo->nombre}}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Pago:</th>
              <td class="text-left">{{$item->tipo_pago->nombre}}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Nomina:</th>
              <td class="text-left">{{$item->nomina->remuneracion}}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Deuda:</th>
              <td class="text-left">{{@$item->deuda->nombre}}</td>
            </tr>
            <!-- Agrega más filas para mostrar otros campos del usuario -->
          </tbody>
        </table>
      </div>

      <div class="table-responsive">
        <h7>Media</h7>
        <table class="table table-bordered table-vertical">
          <tbody>
            <!-- Contenido de la segunda tabla -->
            <tr>
              <th scope="row" class="text-left">Gasto Media Alta</th>
              <td class="text-left">{{$media['alto']}}€</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Gasto Media Baja</th>
              <td class="text-left">{{$media['bajo']}}€</td>
            </tr>
          </tbody>
        </table>
      </div>   
    </div>

    <!-- Div de la derecha -->
    <div class="col-md-6">
      <div class="table-responsive">
        <h7>Suma Gastos</h7>
        <table class="table table-bordered table-vertical">
          <tbody>
            <tr>
              <th scope="row" class="text-left">Tipo</th>
              <td class="text-left">Recibo</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Total:</th>
              <td class="text-left">{{$totalImportes ?? 0}}€</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="row">
        <div class="col-md-12">
          <h7>Historial de pagos</h7>
          <canvas id="myChart"></canvas>
        </div>

        <div class="row">
          <div class="col-md-12">
            <h7>Historial de pagos</h7>
            <canvas id="myChart2"></canvas>
          </div>

          <div class="row">
          <div class="col-md-12">
            <h7>Historial de pagos</h7>
            <canvas id="myChart3"></canvas>
          </div>
         

        </div>
      </div>
      @stop


      @section('js')
      <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
      <script type="text/javascript">
        var importes = <?= json_encode($importes) ?>;

        var fechasLabels = importes.map(function(item) {
          return item.fecha;
          console.log(importes);
        });

        var ctx = document.getElementById('myChart').getContext('2d');

        // Configurar y crear el gráfico de barras
        new Chart(ctx, {
          type: 'bar',
          data: {
            labels: fechasLabels,
            datasets: [{
              label: 'Importe',
              data: importes.map(function(item) {
                return item.importe
              }),
              backgroundColor: 'rgba(54, 162, 235, 0.5)',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              y: {
                beginAtZero: true,
                ticks: {
                  callback: function(value, index, values) {
                    return value + '€'; // Agregar el símbolo del euro al valor del eje Y
                  }
                }
              }
            },
            plugins: {
              tooltip: {
                callbacks: {
                  label: function(context) {
                    var value = context.parsed.y;
                    return value + '€'; // Agregar el símbolo del euro a la etiqueta de información sobre herramientas
                  }
                }
              }
            }
          }
        });
      </script>

      <script type="text/javascript">
        var importes = <?= json_encode($importes) ?>;

        var fechasLabels = importes.map(function(item) {
          return item.fecha;
          console.log(importes);
        });
        var ctx = document.getElementById('myChart2').getContext('2d');

        // Configurar y crear el gráfico de línea
        new Chart(ctx, {
          type: 'line', // Cambiar a tipo 'line' para gráfico de línea
          data: {
            labels: fechasLabels,
            datasets: [{
              label: 'Importe',
              data: importes.map(function(item) {
                return item.importe
              }),
              backgroundColor: 'rgba(54, 162, 235, 0.5)',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              y: {
                beginAtZero: true,
                ticks: {
                  callback: function(value, index, values) {
                    return value + '€'; // Agregar el símbolo del euro al valor del eje Y
                  }
                }
              }
            },
            plugins: {
              tooltip: {
                callbacks: {
                  label: function(context) {
                    var value = context.parsed.y;
                    return value + '€'; // Agregar el símbolo del euro a la etiqueta de información sobre herramientas
                  }
                }
              }
            }
          }
        });
      </script>

<script type="text/javascript">
// Datos del gráfico
var data = {
  labels: [],
  datasets: []
};

// Obtener los datos del JSON y agregarlos al gráfico
var typeLisForm = [
  {
    "data": "2023-07-07",
    "day": "viernes",
    "lisForms": [
      {
        "description": "Petición de privados",
        "total": 1
      }
    ]
  },
  {
    "data": "2023-07-08",
    "day": "sábado",
    "lisForms": [
      {
        "description": "Petición de privados",
        "total": 1
      }
    ]
  },
  {
    "data": "2023-07-09",
    "day": "domingo",
    "lisForms": [
      {
        "description": "default",
        "total": 1
      }
    ]
  },
  {
    "data": "2023-07-11",
    "day": "martes",
    "lisForms": [
      {
        "description": "Petición de privados",
        "total": 1
      },
      {
        "description": "Petición de aseguradora",
        "total": 1
      },
      {
        "description": "Petición de privados",
        "total": 1
      }
    ]
  }
];

typeLisForm.forEach(function(item) {
  // Agregar las descripciones al array de labels
  data.labels.push(item.day);

  // Crear un dataset para cada tipo de formulario
  item.lisForms.forEach(function(form) {
    var dataset = data.datasets.find(function(ds) {
      return ds.label === form.description;
    });

    // Si el dataset ya existe, agregar el total
    if (dataset) {
      dataset.data.push(form.total);
    } else {
      // Si el dataset no existe, crearlo y agregar el total
      data.datasets.push({
        label: form.description,
        data: [form.total],
        backgroundColor: getRandomColor(),
        borderColor: getRandomColor(),
        borderWidth: 1
      });
    }
  });
});

// Configurar y crear el gráfico de barras apiladas
var ctx = document.getElementById('myChart3').getContext('2d');
new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    responsive: true,
    scales: {
      x: {
        stacked: true
      },
      y: {
        stacked: true
      }
    }
  }
});

// Función para obtener un color aleatorio
function getRandomColor() {
  var hue = Math.floor(Math.random() * 360);
  var saturation = Math.floor(Math.random() * 30) + 50; // Rango: 50-80
  var lightness = Math.floor(Math.random() * 20) + 50; // Rango: 50-70

  return `hsl(${hue}, ${saturation}%, ${lightness}%)`;

}
      </script>
      @endsection