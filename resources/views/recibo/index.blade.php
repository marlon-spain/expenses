@extends('adminlte::page')

@section('title', 'items')


@section('content')

<div class="col-lg-12 mt-2">
    <div class="card w-100 h-100">
        <div class="card-body">
            <h7 class="text-right">Registro de recibos: {{ auth()->user()->name }}</h7>

            <br>
            <div class="text-right">
                <a href="{{route('recibo.create')}}" class="btn btn-primary mb-3">Crear Registro</a>
            </div>

            <div class="table-responsive">
                <table id="items" class="table table-hover table-striped table-bordered table-sm" style="width:100%;">
                    <thead>
                        <tr style="text-align: center;">
                            <th scope="col">TimeLine</th>
                            <th scope="col">id</th>
                            <th scope="col">User</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Descripcion</th>
                            <th scope="col">Importe</th>
                            <th scope="col">Cuenta</th>
                            <th scope="col"> Gasto</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Pago</th>
                            <th scope="col">Nomina</th>
                            <th scope="col">Deuda</th>
                            <th scope="col">Accion</th>
                        </tr>

                    </thead>

                </table>
            </div>

        </div>
    </div>
</div>



@stop


@section('js')
@include('layouts.datatable', [
'ajaxUrl' => route('recibo.index'),
'columns' => json_encode([
['data' => 'id'],
['data' => 'id'],
['data' => 'user.name'],
['data' => 'fecha'],
['data' => 'descripcion'],
['data' => 'importe'],
['data' => 'cuenta_id'],
['data' => 'tipo_gasto_id'],
['data' => 'gasto_fijo_id'],
['data' => 'estado_id'],
['data' => 'tipo_pago_id'],
['data' => 'nomina_id'],
['data' => 'deuda_id'],
['data' => 'action', 'orderable' => false],
])
])

@include('layouts.delete', ['deleteUrl' => "recibo"])

@stop