@extends('adminlte::page')

@section('title', 'items')

@section('content_header')
<h1>Listado de Empresas</h1>
@stop

@section('content')

<div class="col-lg-12 mt-2">
    <div class="card w-100 h-100">
        <div class="card-body">
            <h7 class="text-right">Registro de cuentas: {{ auth()->user()->name }}</h7>

            <br>         

            <div class="table-responsive">
                <table id="items" class="table table-hover table-striped table-bordered table-sm" style="width:100%;">
                    <thead>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">user</th>
                            <th scope="col">Route path</th>
                            <th scope="col">Route Method</th>
                            <th scope="col">Route Alias</th>
                            <th scope="col">ip address</th>
                        </tr>

                    </thead>

                </table>
            </div>

        </div>
    </div>
</div>

@stop




@section('js')
@include('layouts.datatable', [
'ajaxUrl' => route('log.index'),
'columns' => json_encode([
['data' => 'id'],
['data' => 'user.name'],
['data' => 'route_path'],
['data' => 'route_method'],
['data' => 'route_alias'],
['data' => 'ip_address'],
])
])

@stop

