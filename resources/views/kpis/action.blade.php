@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
<h4>Kpi's Action Stadistics</h4>
@stop

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="table-responsive">
        <h7>Tabla info User</h7>
        <table class="table table-bordered table-vertical">
          <tbody>
            <!-- Contenido de la primera tabla -->
            <tr>
              <th scope="row" class="text-left">ID:</th>
              <td class="text-left">{{ $user->id }}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Nombre:</th>
              <td class="text-left">{{ $user->name }}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Correo electrónico:</th>
              <td class="text-left">{{ $user->email }}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Equipo actual ID:</th>
              <td class="text-left">{{ $user->current_team_id }}</td>
            </tr>
            <!-- Agrega más filas para mostrar otros campos del usuario -->
          </tbody>
        </table>
      </div>

      <div class="table-responsive">
        <h7>Tabla Nomina</h7>
        <table class="table table-bordered table-vertical">
          <tbody>
            <!-- Contenido de la segunda tabla -->
            <tr>
              <th scope="row" class="text-left">Total Media</th>
              <td class="text-left">{{ $user->nominaMedia }}€</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">deducciones</th>
              <td class="text-left">{{ $user->porcentajeDeducciones }}%</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="table-responsive">
        <h7>Tabla Deuda</h7>
        <table class="table table-bordered table-vertical table table-striped table-hover">
          <tbody>
            <!-- Contenido de la tercera tabla -->
            <tr>
              <th scope="row" class="text-left">Tipo</th>
              <td class="text-left">Deuda</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Total:</th>
              <td class="text-left">{{ $user->deudaCount }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <!-- Div de la derecha -->
    <div class="col-md-6">
      <div class="table-responsive">
        <h7>Tabla Recibo</h7>
        <table class="table table-bordered table-vertical">
          <tbody>
            <tr>
              <th scope="row" class="text-left">Tipo</th>
              <td class="text-left">Recibo</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Total:</th>
              <td class="text-left">{{ $user->reciboCount }}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="row">
        <div class="col-md-6">
          <h7>Uso de Cuentas</h7>
          <canvas id="myChart"></canvas>
        </div>

        <div class="col-md-6">
          <h7>Media de Logs</h7>
          <div class="table-responsive">
            <table class="table table table-bordered table-striped">
              <tbody>
                <!-- Contenido de la segunda tabla -->
                <tr>
                  <th scope="row" class="text-left">Tipo</th>
                  <th scope="row" class="text-left">Total:</th>
                </tr>
                <tr>
                  <td class="text-left">Logs Activity</td>
                  <td class="text-left">{{ $user->logCount }}</td>
                  
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="table-responsive">
        <h7>Tabla info</h7>
        <table class="table table-bordered table-vertical">
          <tbody>
            <tr>
              <th scope="row" class="text-left">Logs</th>
              <td class="text-left">{{ $user->logCount }}</td>
            </tr>
            <tr>
              <th scope="row" class="text-left">Tiempo de session</th>
              <td class="text-left">{{ $user->timeSessionMedia }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    <!--cierre div derecha-->

  </div>
</div>

@stop



@section('js')

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script type="text/javascript">
  var data = <?= json_encode($user) ?>;
  var numericData = data.ratiosUsoCuentas.map(value => parseFloat(value));
  var media = numericData.reduce((a, b) => a + b) / numericData.length;

  new Chart('myChart', {
    type: 'doughnut',
    plugins: [{
      afterDraw: chart => {
        var needleValue = chart.config.data.datasets[0].needleValue;
        var dataTotal = chart.config.data.datasets[0].data.reduce((a, b) => a + b, 0);
        var angle = Math.PI + (1 / dataTotal * needleValue * Math.PI);
        var ctx = chart.ctx;
        var cw = chart.canvas.offsetWidth;
        var ch = chart.canvas.offsetHeight;
        var cx = cw / 2;
        var cy = ch - 6;

        ctx.translate(cx, cy);
        ctx.rotate(angle);
        ctx.beginPath();
        ctx.moveTo(0, -3);
        ctx.lineTo(ch - 20, 0);
        ctx.lineTo(0, 3);
        ctx.fillStyle = 'rgb(0, 0, 0)';
        ctx.fill();
        ctx.rotate(-angle);
        ctx.translate(-cx, -cy);
        ctx.beginPath();
        ctx.arc(cx, cy, 5, 0, Math.PI * 2);
        ctx.fill();
      }
    }, ],
    data: {
      labels: data.ratiosUsoCuentas.slice(0, 2).map((value, index) => `uso : ${value}%`),
      datasets: [{
        data: data.ratiosUsoCuentas,
        needleValue: media,
        backgroundColor: [
          'rgba(0, 123, 255, 0.2)',
          'rgba(0, 0, 255, 0.2)',
          'rgba(255, 99, 132, 0.2)'
        ]
      }]
    },
    options: {
      responsive: false,
      aspectRatio: 2,
      layout: {
        padding: {
          bottom: 3
        }
      },
      rotation: -90,
      cutout: '50%',
      circumference: 180,
      legend: {
        display: false
      },
      animation: {
        animateRotate: true,
        animateScale: true
      },
      plugins: {
        datalabels: {
          color: '#000',
          font: {
            size: 14,
            weight: 'bold'
          }
        }
      }
    }
  });
</script>
@endsection