@extends('adminlte::page')


@section('title', 'KPIS')

@section('content_header')
<h1>KPIs 2022</h1>

@stop

@section('content')


<ul class="nav nav-tabs" id="myTab" role="tablist">


    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">KPI's</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="log-tab" data-toggle="tab" href="#log" role="tab" aria-controls="log" aria-selected="false">Logs</a>
    </li>


</ul>

<div class="tab-content" id="myTabContent">

    <div class="tab-pane fade " id="log" role="tabpanel" aria-labelledby="log-tab">
        @include('kpis.logs')
    </div>


    <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
        @include('kpis.kpis')
    </div>


</div>

@stop




@section('js')

<script>
    $(document).ready(function() {
        $('#home-tab').addClass('active');
    });
</script>


@stop