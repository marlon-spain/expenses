
        <br>
        <div class="card-deck">
            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Total Recibos </strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Total</th>
                                        <td style="color:red;">{{ $item->totalRecibos }}</td>
                                    </tr>
                                    <tr>
                                        <th>Importe</th>
                                        <td style="color:green;">{{ $item->Importe }}€</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Recibos Pagados </strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Total</th>
                                        <td style="color:red;">{{ $pagado->totalRecibos }}</td>
                                    </tr>
                                    <tr>
                                        <th>Importe</th>
                                        <td style="color:green;">{{ $pagado->Importe }}€</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Recibos devueltos </strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Total</th>
                                        <td style="color:red;">{{ $devuelto->totalRecibos }}</td>
                                    </tr>
                                    <tr>
                                        <th>Importe</th>
                                        <td style="color:green;">{{ $devuelto->Importe }}€</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Recibos impagados </strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Total</th>
                                        <td style="color:red;">{{ $impagado->totalRecibos }}</td>
                                    </tr>
                                    <tr>
                                        <th>Importe</th>
                                        <td style="color:green;">{{ $impagado->Importe }}€</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong>Recibos cancelados </strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Total</th>
                                        <td style="color:red;">{{ $cancelado->totalRecibos }}</td>
                                    </tr>
                                    <tr>
                                        <th>Importe</th>
                                        <td style="color:green;">{{ $cancelado->Importe }}€</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Total nominas </strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Total</th>
                                        <td style="color:red;">{{ $nomina->totalNominas }}</td>
                                    </tr>
                                    <tr>
                                        <th>Importe</th>
                                        <td style="color:green;">{{ $nomina->Importe }}€</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <div class="card-deck">

            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top recibos</strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="color:#018BBF;">Estado</th>
                                        <th style="color:#018BBF;">Total recibos</th>
                                        <th style="color:#018BBF;">Importe</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($topRecibos as $top)
                                    <tr>
                                        <td>{{ $top->estado_recibo->nombre }}</td>
                                        <td>{{ $top->totalRecibos }}</td>
                                        <td style="color:green;">{{ $top->Importe }}€</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top nominas</strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="color:#018BBF;">mes</th>
                                        <th style="color:#018BBF;">Total nominas</th>
                                        <th style="color:#018BBF;">Importe</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($topNominas as $top)
                                    <tr>
                                        <td>{{ \Carbon\Carbon::parse(strtotime($top->fecha))->formatLocalized(' %B ')  }}</td>
                                        <td>{{ $top->totalNominas }}</td>
                                        <td style="color:green;">{{ $top->Importe }}€</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top gastos</strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="color:#018BBF;">Tipo</th>
                                        <th style="color:#018BBF;">Total gastos</th>
                                        <th style="color:#018BBF;">Importe</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($topGastos as $top)
                                    <tr>
                                        <td>{{ @$top->gasto_fijo->nombre }}</td>
                                        <td>{{ @$top->totalGastos }}</td>
                                        <td style="color:green;">{{ $top->Importe }}€</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <div class="card-deck">
            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top pagos</strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="color:#018BBF;">Tipo</th>
                                        <th style="color:#018BBF;">Total pagos</th>
                                        <th style="color:#018BBF;">Importe</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($topPagos as $top)
                                    <tr>
                                        <td>{{ $top->tipo_pago->nombre }}</td>
                                        <td>{{ $top->totalPagos }}</td>
                                        <td style="color:green;">{{ $top->Importe }}€</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top deudas</strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="color:#018BBF;">Nombre</th>
                                        <th style="color:#018BBF;">Estado</th>
                                        <th style="color:#018BBF;">Total</th>
                                        <th style="color:#018BBF;">Restante</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($topdeudas as $top)
                                    <tr>
                                        <td>{{ $top->nombre }}</td>
                                        <td>{{ $top->estado->nombre }}</td>
                                        <td style="color:red;">{{ $top->total }}€</td>
                                        <td style="color:green;">{{ $top->restante }}€</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="card-body">
                        <h4 class="card-title col-sm-12 d-flex justify-content-center "><strong> Top estados</strong></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="color:#018BBF;">Nombre</th>
                                        <th style="color:#018BBF;">Total estado</th>
                                        <th style="color:#018BBF;">Importe</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($topEstados as $top)
                                    <tr>
                                        <td>{{ $top->estado_recibo->nombre }}</td>
                                        <td>{{ $top->totalEstados }}</td>
                                        <td style="color:green;">{{ $top->Importe }}€</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
