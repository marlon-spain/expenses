<br>
<div class="card-deck">
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Total Logs </strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Total</th>
                                <td style="color:red;">{{ $log->totalLogs }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Total Users </strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Total</th>
                                <td style="color:red;">{{ $log7->totalUsers }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Total Roles </strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Total</th>
                                <td style="color:red;">{{ $log8->totalRoles }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Total Permisos </strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Total</th>
                                <td style="color:red;">{{ $log9->totalPermisos }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="card-deck">
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> GET </strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Metodo</th>
                                <td style="color:#018BBF;">{{ $log2->route_method }}</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td style="color:red;">{{ $log2->totalLogs }}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> POST </strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Metodo</th>
                                <td style="color:green;">{{ $log3->route_method }}</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td style="color:red;">{{ $log3->totalLogs }}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> PUT </strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Metodo</th>
                                <td style="color:orange;">{{ $log4->route_method }}</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td style="color:red;">{{ $log4->totalLogs }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> PATCH </strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Metodo</th>
                                <td style="color:#13FFE2;">{{ $log5->route_method }}</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td style="color:red;">{{ $log5->totalLogs }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> DELETE </strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Metodo</th>
                                <td style="color:red;">{{ $log6->route_method }}</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td style="color:red;">{{ $log6->totalLogs }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="card-deck">

    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top Metodos</strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="color:#018BBF;">Metodo</th>
                                <th style="color:#018BBF;">Total</th>


                            </tr>
                        </thead>
                        <tbody>
                            @foreach($log10 as $top)
                            <tr>
                                <td>{{ $top->route_method }}</td>
                                @if($top->route_method == 'GET')
                                <td style="color:#018BBF;">{{ $top->totalMetodos }}</td>
                                @endif
                                @if($top->route_method == 'POST')
                                <td style="color:green;">{{ $top->totalMetodos }}</td>
                                @endif
                                @if($top->route_method == 'PUT')
                                <td style="color:orange;">{{ $top->totalMetodos }}</td>
                                @endif
                                @if($top->route_method == 'DELETE')
                                <td style="color:red;">{{ $top->totalMetodos }}</td>
                                @endif
                                @if($top->route_method == 'PATCH')
                                <td style="color:#13FFE2;">{{ $top->totalMetodos }}</td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top Rutas</strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="color:#018BBF;">Rutas</th>
                                <th style="color:#018BBF;">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($log11 as $top)
                            <tr>
                                <td>{{ $top->route_path }}</td>
                                <td style="color:green;">{{ $top->totalRutas}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top Alias</strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="color:#018BBF;">Alias</th>
                                <th style="color:#018BBF;">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($log12 as $top)
                            <tr>
                                <td>{{ $top->route_alias }}</td>
                                <td style="color:green;">{{ $top->totalAlias}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>   
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top Navegadores</strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="color:#018BBF;">Navegador</th>
                                <th style="color:#018BBF;">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($log14 as $top)
                            @php
                            $split = explode(" ", $top->user_agent); 
                            $lastWord = $split[count($split)-1];
                            @endphp
                            <tr>
                                <td>{{ $lastWord }}</td>
                                <td style="color:green;">{{ $top->totalAgent}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top User</strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="color:#018BBF;">User</th>
                                <th style="color:#018BBF;">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($log15 as $top)
                            <tr>
                                @if(@$top->user->name == '')
                                <td style="color:red;">sin User</td>
                                @else
                                <td>{{@$top->user->name}}</td>
                                @endif
                                <td style="color:green;">{{ $top->totalUser}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Top IP</strong></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="color:#018BBF;">Address</th>
                                <th style="color:#018BBF;">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($log13 as $top)
                            <tr>
                                <td>{{ $top->ip_address }}</td>
                                <td style="color:green;">{{ $top->totalIp}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>