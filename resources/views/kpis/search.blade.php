@extends('adminlte::page')
@section('title', 'kpis')

@section('content')
<div class="container-fluid">
    <h3 class="page_heading d-flex align-items-center justify-content-center">Key Performance Indicators</h3>
    <div class="sticky-top">

        @if ($errors->any())
        <div class="alert alert-dark alert-dismissible fade show" role="alert">
            <strong>Revise los campos!</strong>
            @foreach($errors->all() as $error)
            <span class="badge badge-danger">{{$error}}</span>
            @endforeach
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <form class="form-inline bg-transparent" id="buscador" method="POST" action="{{ route('kpis.searchFilter') }}">
            @csrf
            <div class="form-group mr-2">
                <input type="text" id="search" name="search" class="form-control" placeholder="inserta nombre del Recibo" />
            </div>
            <div class="form-group mr-2">
                <input type="text" id="date" name="date" class="form-control" placeholder="Seleccionar fecha" />
            </div>
            <button type="submit" class="btn btn-primary ml-auto">Buscar</button>
        </form>
    </div>

    @if(isset($data))

    <div class="row">
        <div class="col-12">
            <div class="chart-container">
                <canvas id="container" class="full-width-chart"></canvas>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="chart-container">
                <canvas id="container2" class="full-width-chart"></canvas>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="chart-container">
                <canvas id="container3" class="full-width-chart"></canvas>
            </div>
        </div>
    </div>
    @else
    <div class="col-12">
        no existe resultado
    </div>
    @endif
</div>
@stop

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
@stop

@section('js')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>

<script>
    var url = "{{ route('kpis.searchFilter') }}"; // Obtén la URL del endpoint para la búsqueda
    var chartBar = null; // Variable para almacenar la instancia del gráfico de barras
    var chartLine = null; // Variable para almacenar la instancia del gráfico de línea
    var chartHorizontBar = null;

    $(document).ready(function() {
        $('#buscador').submit(function(e) {
            e.preventDefault(); // Prevenir envío predeterminado del formulario
            var searchUrl = url; // Obtén la URL de búsqueda
            var formData = $(this).serialize(); // Serializa los datos del formulario
            $.ajax({
                url: searchUrl,
                method: 'POST',
                data: formData, // Envía los datos del formulario serializados
                success: function(response) {
                    createChart('container', 'bar', response);
                    createChart('container2', 'line', response);
                    createHorizontChart('container3', 'bar', response);

                },
                error: function(xhr) {
                    console.log(xhr.responseText); // Maneja cualquier error de solicitud
                }
            });
        });

        var data = <?= json_encode($data) ?>;
        createChart('container', 'bar', data);
        createChart('container2', 'line', data);
        createHorizontChart('container3', 'bar', data);
    });

    function createChart(containerId, chartType, data) {
        console.log(data);
        var labels = data.map(item => item.fecha);
        var values = data.map(item => item.y);
        var ctx = document.getElementById(containerId).getContext('2d');

        // Destruye el gráfico existente si ya existe una instancia
        if (chartType === 'bar' && chartBar !== null) {
            chartBar.destroy();
        } else if (chartType === 'line' && chartLine !== null) {
            chartLine.destroy();
        }
        else if (chartType === 'bar' && chartHorizontBar !== null) {
            chartHorizontBar.destroy();
        }

        var chart = new Chart(ctx, {
            type: chartType,
            data: {
                labels: labels,
                datasets: [{
                    label: 'total',
                    data: values,
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true, // Permite que el gráfico se ajuste al tamaño del contenedor
                maintainAspectRatio: false, // Desactiva el mantenimiento del ratio de aspecto
                aspectRatio: 1.2, // Ajusta el valor según el ratio de aspecto deseado
                scales: {
                    y: {
                        beginAtZero: true,
                        ticks: {
                            stepSize: 12 // Ajusta el valor según el espaciado y la altura deseada del gráfico
                        }
                    }
                },
            }
        });

        // Asigna la instancia del gráfico correspondiente
        if (chartType === 'bar') {
            chartBar = chart;
        } else if (chartType === 'line') {
            chartLine = chart;
        }
        else if (chartType === 'bar') {
            chartHorizontBar = chart;
        }
    }
</script>

<script>
        var chartLine = null; // Variable para almacenar la instancia del gráfico de línea

    function createHorizontChart(containerId, chartType, data) {
        console.log(data);
        var labels = data.map(item => item.fecha);
        var values = data.map(item => item.y);
        var ctx = document.getElementById(containerId).getContext('2d');

        // Destruye el gráfico existente si ya existe una instancia       
       
         if (chartType === 'bar' && chartHorizontBar !== null) {
            chartHorizontBar.destroy();
        }

        var chart = new Chart(ctx, {
            type: chartType,
            data: {
                labels: labels,
                datasets: [{
                    label: 'total',
                    data: values,
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true, // Permite que el gráfico se ajuste al tamaño del contenedor
                maintainAspectRatio: false, // Desactiva el mantenimiento del ratio de aspecto
                aspectRatio: 1.2, // Ajus
                indexAxis: 'y',
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each horizontal bar to be 2px wide
                elements: {
                    bar: {
                        borderWidth: 2,
                    }
                },
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Horizontal Bar Chart'
                    }
                }
            },
        });
       
         if (chartType === 'bar') {
            chartHorizontBar = chart;
        }
    }
</script>
@stop