@extends('adminlte::page')

@section('title', 'items')


@section('content')
<div class="col-lg-12 mt-2">
    <div class="card w-100 h-100">
        <div class="card-body">
            <h7 class="text-right">Registro de recibos deudas: {{ auth()->user()->name }}</h7>
            <div class="text-right">
                <a href="{{route('deuda.create')}}" class="btn btn-primary mb-3">Crear Registro</a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-striped table-bordered table-sm" id="items" style="width:100%;">
                    <thead>
                        <tr style="text-align: center;">
                            <th scope="col">id</th>
                            <th scope="col">user</th>
                            <th scope="col">Fecha_inicio</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Fecha fin</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Total</th>
                            <th scope="col">Restante</th>
                            <th scope="col">Progreso</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>

<!--modal detalle-->





@stop

@section('js')

@include('layouts.datatable', [
'ajaxUrl' => route('deuda.index'),
'columns' => json_encode([
['data' => 'id'],
['data' => 'user.name'],
['data' => 'fecha_inicio'],
['data' => 'nombre'],
['data' => 'fecha_fin'],
['data' => 'total'],
['data' => 'restante'],
['data' => 'estado_deuda_id'],
['data' => 'Proceso_deuda_id'],
['data' => 'action', 'orderable' => false],
])
])

@include('layouts.delete', ['deleteUrl' => "deuda"])

@stop