@extends('adminlte::page')
@section('title', 'deudas')

@section('content')
<section class="section">
    <div class="section-header">
        <h3 class="page_heading">Alta de deudas</h3>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">


                        @if ($errors->any())
                        <div class="alert alert-dark alert-dismissible fade show" deuda="alert">
                            <strong>Revise los campos!</strong>
                            @foreach($errors->all() as $error)
                            <span class="badge badge-danger">{{$error}}</span>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        {!! Form::open(array('route'=>'deuda.store', 'method'=>'POST')) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                    <label for="name">Fecha_Inicio</label>
                                    {!! Form::date('fecha_inicio', null, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    {!! Form::text('nombre', null, array('class'=>'form-control', 'placeholder'=>'Nombre')) !!}
                                </div>

                                <div class="form-group">
                                    <label for="name">Fecha_fin</label>
                                    {!! Form::date('fecha_fin', null, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                            {!! Form::label('estado_deuda_id', 'Estado', ['class' =>'form-label']) !!}
                            <select name="estado_deuda_id" class="form-control">
                                @foreach($deuda as $d)
                                 <option value="{{$d->id}}">{{$d->nombre}}</option>                        
                                @endforeach
                            </select>
                                </div>

                                <div class="form-group">
                                    <label for="name">Dedua Total</label>
                                    {!! Form::number('total',null,['class' => 'form-control','step'=>'any', 'placeholder'=>'Deuda total']) !!}
                                </div>

                                <div class="form-group">
                                    <label for="name">Deuda Restante</label>
                                    {!! Form::number('restante',null,['class' => 'form-control','step'=>'any', 'placeholder'=>'Deuda restante']) !!}
                                </div>

                                <div class="form-group">
                            {!! Form::label('Proceso_deuda_id', 'Proceso', ['class' =>'form-label']) !!}
                            <select name="Proceso_deuda_id" class="form-control">
                                @foreach($proceso as $p)
                                 <option value="{{$p->id}}">{{$p->nombre}}</option>                        
                                @endforeach
                            </select>
                                </div>                            
                               

                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}



                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


@stop


