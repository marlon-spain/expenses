<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'amb') }}</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <!--- Scripts --->
    <script src="{{ mix('js/app.js') }}" defer data-turbolinks-suppress-warning></script>

</head>

<body class="font-sans antialiased">
    <x-jet-banner />

    <div class="min-h-screen bg-gray-100">
        @livewire('navigation-menu')

        <!-- Page Content -->
        <main>   

            <div id="contenido-ajax">
                <!-- Contenido que se cargará de forma asíncrona -->
            </div>
            {{ $slot }}
        </main>
    </div>

    @stack('modals')

    @livewireScripts

        <!-- Script de AJAX -->
    <script>
    $(document).on('click', 'a', function(e) {
        e.preventDefault(); // Evita la acción de navegación predeterminada

        var url = $(this).attr('href'); // Obtén la URL del enlace

        // Realiza una solicitud AJAX para cargar el contenido
        $.ajax({
            url: url,
            method: 'GET',
            success: function(response) {
                $('#contenido-ajax').html(response); // Actualiza el contenido en el contenedor
            },
            error: function(xhr) {
                console.log(xhr.responseText); // Maneja cualquier error de solicitud
            }
        });
    });
</script>


</body>


</html>
