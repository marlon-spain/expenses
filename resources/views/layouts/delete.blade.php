<script>
    $(document).ready(function() {
        // Capturar clic en el botón de eliminación
        $(document).on('click', '.delete', function() {
            var itemId = $(this).data('id'); // Obtener el ID del elemento

            if (confirm('¿Estás seguro de eliminar este elemento?')) {
                deleteItem(itemId); // Llamar a la función para eliminar el elemento
            }
        });
    });

    function deleteItem(itemId) {
        //console.log('{{ $deleteUrl }}')
        
        $.ajax({
            url: '/{{ $deleteUrl }}/' + itemId, // Actualiza la URL con el ID del elemento
            type: 'DELETE',
            data: {
                '_token': $('input[name="_token"]').val()
            },
            success: () => {
                // Eliminación exitosa
                alert('El elemento ha sido eliminado correctamente');

                // Actualizar la tabla DataTable
                $('#items').DataTable().ajax.reload();
            },
            error: (xhr, status, error) => {
                // Error al eliminar el elemento
                alert('Se produjo un error al eliminar el elemento');
                console.error(error);
            }
        });
    }
</script>