
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#items').DataTable({
            "processing": true,
            "serverSide": true,
            responsive: true,
            "scrollX": true,
            autowith: false,
            "ordering": 'true',
            "order": [0, 'desc'],
            "ajax": '{{ $ajaxUrl }}',
            "columns":<?= $columns ?>
        });
    });


</script>


<script>
    /*
    $(document).ready(function() {
        $('#items').DataTable({
            responsive: true,
            "scrollX": true,
            autowith: false,
            "ordering": 'true',
            "order": [0, 'desc'],
            // "lengthMenu":[[5,10,50, -1],[5,10,50, "All"]]
            "language": {
                "lengthMenu": "Mostrar  " +
                    `<select class="custom-select-sm custom-select-sm form-control form-control-sm">
                          <option value=   '5'>  5</option>
                          <option value=  '10'> 10</option>
                          <option value=  '25'> 25</option>
                          <option value=  '50'> 50</option>
                          <option value= '100'>100</option>
                          <option value=  '-1'>All</option>
                          </select>` +
                    " paginas",
                "zeroRecords": "Nada encontrado - disculpas",
                "info": "Mostrando la pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "( Filtrado de _MAX_ registros totales)",
                'search': 'Buscar:',
                'paginate': {
                    'next': 'siguiente',
                    'previous': 'anterior'
                }
            }
        });
    });
    */
</script>