<!-- extiende del modal -->

@foreach ($items as $item)
<!--MODAL -->
<div class="modal fade" id="myModal-{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

  <div class="modal-dialog modal-lg modal-dialog-centered">  <!--modal-sm modal-xl  modal-lg-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Deuda {{$item->fecha}}</h5>
  

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <table class="table table-striped">
    <thead>
      <tr>
        <th>user</th>
        <th>Fecha_inicio</th>
        <th>Nombre</th>
        <th>Fecha_fin</th>
        <th>Estado</th>
        <th>Progreso</th>
          
      </tr>
    </thead>
    <tbody>     
      <tr>
     
        <td>{{$item->user->name}}</td>
        <td>{{$item->fecha_inicio}}</td>
        <td>{{$item->nombre}}</td>
        <td>{{$item->fecha_fin}}</td>
        <td>{{@$item->estado->nombre}}</td>
        <td>{{$item->proceso->nombre}}</td>
       
      </tr>
    </tbody>
  </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>  
</div>
<!-- /. modal content-->
@endforeach
