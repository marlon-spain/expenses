@extends('adminlte::page')
@section('title', 'deudas')

@section('content')
<section class="section">
    <div class="section-header">
        <h3 class="page_heading">Actualizar campos</h3>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">


                        @if ($errors->any())
                        <div class="alert alert-dark alert-dismissible fade show" deuda="alert">
                            <strong>Revise los campos!</strong>
                            @foreach($errors->all() as $error)
                            <span class="badge badge-danger">{{$error}}</span>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        {!! Form::model($edit, ['route' => ['deuda.update', $edit->id], 'method' => 'PUT']) !!}

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="name">Fecha_Inicio</label>
                                    {!! Form::date('fecha_inicio', $edit->fecha_inicio, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    {!! Form::text('nombre', $edit->nombre, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    <label for="name">Fecha_fin</label>
                                    {!! Form::date('fecha_fin', $edit->fecha_fin, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('Estado:  '. $edit->estado->nombre) !!}
                                    <select name="estado_deuda_id" class="form-control">
                                        @foreach($deuda as $d)
                                        <option value="{{$d->id}}">{{$d->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('proceso: '. $edit->proceso->nombre) !!}
                                    <select name="Proceso_deuda_id" class="form-control">
                                        @foreach($proceso as $p)
                                        <option value="{{$p->id}}">{{$p->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}



                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


@stop