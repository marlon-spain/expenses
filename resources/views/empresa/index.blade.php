@extends('adminlte::page')

@section('title', 'items')

@section('content_header')
<h1>Listado de Empresas</h1>
@stop

@section('content')

<div class="col-lg-12 mt-2">
    <div class="card w-100 h-100">
        <div class="card-body">
            <h7 class="text-right">Registro de cuentas: {{ auth()->user()->name }}</h7>

            <br>
            <div class="text-right">
                <a href="{{route('cuenta.create')}}" class="btn btn-primary mb-3">Crear Registro</a>
            </div>

            <div class="table-responsive">
                <table id="items" class="table table-hover table-striped table-bordered table-sm" style="width:100%;">
                    <thead>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">nombre</th>
                            <th scope="col">creacion</th>                          
                            <th scope="col">acciones</th>

                        </tr>

                    </thead>

                </table>
            </div>

        </div>
    </div>
</div>

@stop

@section('js')

@include('layouts.datatable', [
'ajaxUrl' => route('empresa.index'),
'columns' => json_encode([
['data' => 'id'],
['data' => 'nombre'],
['data' => 'created_at'],

['data' => 'action', 'orderable' => false],
])
])

@include('layouts.delete', ['deleteUrl' => "empresa"])


@stop