@extends('adminlte::page')

@section('title', 'items')

@section('content_header')
<h1>Listado de Empresas</h1>
@stop

@section('content')

<script type=”text/javascript” src=”jquery.min.js”></script>

<link rel=”stylesheet” type=”text/css” href=”toastr/toastr.css”>

<script type=”text/javascript” src=”toastr/toastr.js”></script>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>


    <div class="container">

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Lista de empresa</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Nuevo Empresa</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Grafica</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <h3>Lista empresa</h3>
                <table id="items" class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">nombre</th>
                            <th scope="col">creacion</th>
                            <th scope="col">acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <h3>Nueva Empresa</h3>
                <form id="registro-empresa" action="{{ route('empresa.store') }}" method="post">
                @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp" placeholder="Enter nombre">
                        
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>

            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <h3>Graficos</h3>
            </div>
        </div>






    </div> <!--fin container-->


</body>

@stop

@section('js')

<script>
    //listar
    $(document).ready(function() {
        var TablaEmpresa = $('#items').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('empresa.index') }}",
            },
            columns: [{
                    data: 'id'
                },
                {
                    data: 'nombre'
                },
                {
                    data: 'created_at'
                },
                {
                    data: 'action',
                    ordertable: false
                }
            ]
        });
    });
</script>


<script>

    //crear
$('registro-empresa').submit(function(e){
     e.preventDefault();

     var nombre = $('#nombre').val();

     $.ajax({
        url: "{{ route('empresa.store') }}",
         type:"POST",
         data:{
             nombre: nombre,
          
         },
         succes:function(response){
              if(response){
                  $('#registro-empresa')[0].reset();
                 toastr.success( 'El registro se ingreso correctamente.','nuevo registro', {timeOut:3000});
                  $('#items').DataTable().ajax.reload();
              }
         }
     });

});

</script>

<script>
    //eliminar
    $(".deleteProduct").click(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax(
    {
        url: "user/delete/"+id,
        type: 'delete', // replaced from put
        dataType: "JSON",
        data: {
            "id": id // method and token not needed in data
        },
        success: function (response)
        {
            console.log(response); // see the reponse sent
        },
        error: function(xhr) {
         console.log(xhr.responseText); // this line will save you tons of hours while debugging
        // do something here because of error
       }
    });
});
</script>


@stop