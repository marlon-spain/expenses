@extends('adminlte::page')
@section('title', 'user')

@section('content')
<section class="section">
    <div class="section-header">
        <h3 class="page_heading">Actualizar campos</h3>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">


                        @if ($errors->any())
                        <div class="alert alert-dark alert-dismissible fade show" deuda="alert">
                            <strong>Revise los campos!</strong>
                            @foreach($errors->all() as $error)
                            <span class="badge badge-danger">{{$error}}</span>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'PUT']) !!}

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    {!! Form::text('name', null, array('class'=>'form-control')) !!}
                                </div>


                                <div class="form-group">
                                    <label for="email">Email</label>
                                    {!! Form::email('email', null, array('class'=>'form-control')) !!}
                                </div>                                

                                <div class="form-group">
                                    <label for="password">Password</label><br>
                                    {!! Form::password('password', null, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    <label for="confirm-password">Confirm-password</label><br>
                                    {!! Form::password('confirm-password', null, array('class'=>'form-control')) !!}
                                </div>


                                <div class="form-group">
                                    <label for="">Roles</label>
                                    {!! Form::select('roles[]', $roles,$userRole, array('class'=>'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    <label for="">Permisos</label><br>
                                    @foreach($permissions as $permission)
                                    <i>{{ $permission }};</i>
                                    
                                    @endforeach
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn btn-primary">update</button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}



                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


@stop