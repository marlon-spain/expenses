@extends('adminlte::page')

@section('title', 'items')

@section('content_header')
<meta name="csrf-token" content="{{ csrf_token() }}">

@stop

@section('content')
<div class="card-deck">
    <div class="card">
        <div class="card-block">
            <div class="card-body">
                <h4 class="card-title col-sm-12 d-flex justify-content-center"><strong> Calendario </strong></h4>

                <div class="table-responsive">
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('css')
<link href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.css" />
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" />
@stop

@section('js')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        // pass _token in all ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

        });

        // initialize calendar in all events
        var calendar = $('#calendar').fullCalendar({
            defaultView: 'month', // Vista predeterminada: mes
            height: 'auto', // Altura automática del calendario

            // Otras opciones de diseño
            contentHeight: 'auto', // Altura automática del contenido del calendario
            aspectRatio: 2.5, // Relación de aspecto (ancho:alto) del calendario
            eventLimit: true, // Mo
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            navLinks: true,
            editable: true,
            editable: true,
            eventLimit: true,
            responsive: true,
            events: "{{ route('calendar.index') }}",
            displayEventTime: true,
            eventRender: function(event, element, view) {
                if (event.allDay === 'true') {
                    event.allDay = true;
                } else {
                    event.allDay = false;
                }
            },
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                var event_name = prompt('Event Name:');
                if (event_name) {
                    var start = $.fullCalendar.formatDate(start, "YYYY-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(end, "YYYY-MM-DD HH:mm:ss");
                    $.ajax({
                        url: "{{ route('calendar.create') }}",
                        data: {
                            title: event_name,
                            start: start,
                            end: end,
                        },
                        type: 'post',
                        success: function(data) {
                            iziToast.success({
                                position: 'topRight',
                                message: 'Event created successfully.',
                            });

                            calendar.fullCalendar('renderEvent', {
                                id: data.id,
                                title: event_name,
                                start: start,
                                end: end,
                                allDay: allDay
                            }, true);
                            calendar.fullCalendar('unselect');
                        }
                    });
                }
            },
            eventDrop: function(event, delta) {
                var start = $.fullCalendar.formatDate(event.start, "YYYY-MM-DD HH:mm:ss");
                var end = $.fullCalendar.formatDate(event.end, "YYYY-MM-DD HH:mm:ss");

                $.ajax({
                    url: "{{ route('calendar.edit') }}",
                    data: {
                        title: event.event_name,
                        start: start,
                        end: end,
                        id: event.id,
                    },
                    type: "POST",
                    success: function(response) {
                        iziToast.success({
                            position: 'topRight',
                            color: 'blue',
                            message: 'Event updated successfully.',
                        });
                    }
                });
            },
            eventClick: function(event) {
                var eventDelete = confirm('Are you sure to remove event?');
                if (eventDelete) {
                    $.ajax({
                        type: "post",
                        url: "{{ route('calendar.destroy') }}",
                        data: {
                            id: event.id,
                            _method: 'delete',
                        },
                        success: function(response) {
                            calendar.fullCalendar('removeEvents', event.id);
                            iziToast.success({
                                position: 'topRight',
                                color: 'red',
                                message: 'Event removed successfully.',
                            });
                        }
                    });
                }
            }
        });
    });
</script>
@stop