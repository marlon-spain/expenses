@extends('adminlte::page')
@section('title', 'Roles')

@section('content')
<section class="section">
    <div class="section-header">
        <h3 class="page_heading">Editar Roles</h3>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">


                        @if ($errors->any())
                        <div class="alert alert-dark alert-dismissible fade show" role="alert">
                            <strong>Revise los campos!</strong>
                            @foreach($errors->all() as $error)
                            <span class="badge badge-danger">{{$error}}</span>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        {!! Form::model($role, ['method'=>'PATCH','route'=>['roles.update',$role->id]]) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="name">Nombre del Rol</label>
                                    {!! Form::text('name', null, array('class'=>'form-control')) !!}
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="name">Permisos para este Rol</label>
                                        <br>
                                      
                                        @foreach($permission as $value)
                                 <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}

    
                                            {{ $value->name }} </label>
                                        <br/>
                                        @endforeach
                                    </div>

                                </div>

                         
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                            
                            </div>
                        </div>

                        {!! Form::close() !!}



                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')



<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#roles').DataTable({
            responsive: true,
            autowith: false,
            // "lengthMenu":[[5,10,50, -1],[5,10,50, "All"]]
            "language": {
                "lengthMenu": "Mostrar  " +
                    `<select class="custom-select-sm custom-select-sm form-control form-control-sm">
                          <option value=   '5'>  5</option>
                          <option value=  '10'> 10</option>
                          <option value=  '25'> 25</option>
                          <option value=  '50'> 50</option>
                          <option value= '100'>100</option>
                          <option value=  '-1'>All</option>
                          </select>` +
                    "  Registros por paginas",
                "zeroRecords": "Nada encontrado - disculpas",
                "info": "Mostrando la pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "( Filtrado de _MAX_ registros totales)",
                'search': 'Buscar:',
                'paginate': {
                    'next': 'siguiente',
                    'previous': 'anterior'
                }
            }
        });
    });
</script>

@stop