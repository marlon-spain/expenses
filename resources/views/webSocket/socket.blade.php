<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel Broadcast Redis Socket io Tutorial - ItSolutionStuff.com</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
</head>

<body>

    <div class="container">
        <table id="roles" class="table table-striped">
            <thead>
                <h1>{{ Request::getHost() }} -- {{ env('LARAVEL_ECHO_PORT') }} Event Creator</h1>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Type</th>
                    <th scope="col">App/Puerto</th>
                    <th scope="col">Details</th>
                    <th scope="col">Time</th>
                </tr>
            </thead>
            <tbody id="notification">
            </tbody>
        </table>
    </div>

</body>

<script>
    window.laravel_echo_port = '{{ env("LARAVEL_ECHO_PORT") }}';
</script>
<script src="//{{ Request::getHost() }}:{{ env('LARAVEL_ECHO_PORT') }}/socket.io/socket.io.js"></script>
<script src="{{ url('/js/laravel-echo-setup.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#roles').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "language": {
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Previous"
                }
            }
        });
        var i = 0;
        window.Echo.channel('user-channel')
            .listen('.UserEvent', (data) => {
                console.log(data);
                i++;
                var row = $('<tr>');
                row.append($('<td>').text(i));
                row.append($('<td>').text(data.type));
                row.append($('<td>').text(data.appPuerto));
                row.append($('<td>').text(data.details));
                row.append($('<td>').text(data.time));
                table.row.add(row).draw();
            });

        window.Echo.channel('public-message-channel')
            .listen('.MessageEvent', (data) => {
                console.log(data);
                i++;
                var row = $('<tr>');
                row.append($('<td>').text(i));
                row.append($('<td>').text(data.type));
                row.append($('<td>').text(data.appPuerto));
                row.append($('<td>').text(data.details));
                row.append($('<td>').text(data.time));
                table.row.add(row).draw();
            });    

     /*   window.Echo.private('casilla-channel.' + 1)
            .listen('.CasillaEvent', (data) => {
                console.log("recibido");
                i++;
                var row = $('<tr>');
                row.append($('<td>').text(i));
                row.append($('<td>').text(data.type));
                row.append($('<td>').text(data.appPuerto));
                row.append($('<td>').text(data.details));
                row.append($('<td>').text(data.time));
                table.row.add(row).draw();
            });
        */
    });
</script>

</html>