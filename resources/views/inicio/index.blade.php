@extends('adminlte::page')

@section('title', 'Roles')

@section('content')
<div class="container">
  <h6>Recibos detallados con estadísticas</h6>

  <!-- Estadísticas -->
  <div class="row mb-2">
    <h7>Tus estadísticas</h7>
    <div class="card">
      <div class="row">
        <div class="col-md-4">
          <div class="chart-container">
            <canvas id="myChart1" width="150" height="80"></canvas>
          </div>
        </div>

        <div class="col-md-4">
          <div class="chart-container">
            <canvas id="myChart2" width="150" height="100"></canvas>
          </div>
        </div>

        <div class="col-md-4">
          <div class="chart-container">
            <canvas id="myChart3" width="150" height="100"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Estadísticas de Recibos -->
  <div class="row mb-2">
    <h7>Estadísticas de Recibos</h7>
    <div class="card">
      <div class="row">
        <div class="col-md-4">
          <h9>Afluencia (90 días)</h9>
          <div class="chart-container">
            <canvas id="myChart4" width="150" height="100"></canvas>
          </div>
        </div>

        <div class="col-md-4">
          <h9>Registro de peticiones semanal</h9>
          <div class="chart-container">
            <canvas id="myChart5" width="150" height="100"></canvas>
          </div>
        </div>

        <div class="col-md-4">
          <h9>Afluencia (90 días)</h9>
          <div class="chart-container">
            <canvas id="myChart6" width="150" height="100"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Estadísticas de Nominas -->
  <div class="row mb-2">
    <h7>Estadísticas de Nominas</h7>
    <div class="card">
      <div class="row">
        <div class="col-md-4">
          <div class="chart-container">
            <canvas id="myChart7" width="150" height="100"></canvas>
          </div>
        </div>

        <div class="col-md-4">
          <div class="chart-container">
            <canvas id="myChart8" width="150" height="100"></canvas>
          </div>
        </div>

        <div class="col-md-4">
          <div class="chart-container">
            <canvas id="myChart9" width="150" height="100"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop


@section('js')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script>
  var ctx = document.getElementById('myChart1').getContext('2d');
  var myPieChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: [],
      datasets: [{
        label: 'Datos de ejemplo',
        data: [10, 20, 15],
        backgroundColor: ['red', 'blue', 'green']
      }]
    },
    options: {
      responsive: true
    }
  });
</script>



<script type="text/javascript">
  // Datos del gráfico
  var data = {
    labels: [],
    datasets: []
  };

  // Obtener los datos del JSON y agregarlos al gráfico
  var typeLisForm = [{
      "data": "2023-07-07",
      "day": "viernes",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-08",
      "day": "sábado",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-09",
      "day": "domingo",
      "lisForms": [{
        "description": "default",
        "total": 1
      }]
    },
    {
      "data": "2023-07-11",
      "day": "martes",
      "lisForms": [{
          "description": "Petición de privados",
          "total": 1
        },
        {
          "description": "Petición de aseguradora",
          "total": 1
        },
        {
          "description": "Petición de privados",
          "total": 1
        }
      ]
    }
  ];

  typeLisForm.forEach(function(item) {
    // Agregar las descripciones al array de labels
    data.labels.push(item.day);

    // Crear un dataset para cada tipo de formulario
    item.lisForms.forEach(function(form) {
      var dataset = data.datasets.find(function(ds) {
        return ds.label === form.description;
      });

      // Si el dataset ya existe, agregar el total
      if (dataset) {
        dataset.data.push(form.total);
      } else {
        // Si el dataset no existe, crearlo y agregar el total
        data.datasets.push({
          label: form.description,
          data: [form.total],
          backgroundColor: getRandomColor(),
          borderColor: getRandomColor(),
          borderWidth: 1
        });
      }
    });
  });

  // Configurar y crear el gráfico de barras apiladas
  var ctx = document.getElementById('myChart2').getContext('2d');

  new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
      responsive: true,
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true
        }
      }
    }
  });

  // Función para obtener un color aleatorio
  function getRandomColor() {
    var hue = Math.floor(Math.random() * 360);
    var saturation = Math.floor(Math.random() * 30) + 50; // Rango: 50-80
    var lightness = Math.floor(Math.random() * 20) + 50; // Rango: 50-70

    return `hsl(${hue}, ${saturation}%, ${lightness}%)`;

  }
</script>

<script type="text/javascript">
  // Datos del gráfico
  var data = {
    labels: [],
    datasets: []
  };

  // Obtener los datos del JSON y agregarlos al gráfico
  var typeLisForm = [{
      "data": "2023-07-07",
      "day": "viernes",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-08",
      "day": "sábado",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-09",
      "day": "domingo",
      "lisForms": [{
        "description": "default",
        "total": 1
      }]
    },
    {
      "data": "2023-07-11",
      "day": "martes",
      "lisForms": [{
          "description": "Petición de privados",
          "total": 1
        },
        {
          "description": "Petición de aseguradora",
          "total": 1
        },
        {
          "description": "Petición de privados",
          "total": 1
        }
      ]
    }
  ];

  typeLisForm.forEach(function(item) {
    // Agregar las descripciones al array de labels
    data.labels.push(item.day);

    // Crear un dataset para cada tipo de formulario
    item.lisForms.forEach(function(form) {
      var dataset = data.datasets.find(function(ds) {
        return ds.label === form.description;
      });

      // Si el dataset ya existe, agregar el total
      if (dataset) {
        dataset.data.push(form.total);
      } else {
        // Si el dataset no existe, crearlo y agregar el total
        data.datasets.push({
          label: form.description,
          data: [form.total],
          backgroundColor: getRandomColor(),
          borderColor: getRandomColor(),
          borderWidth: 1
        });
      }
    });
  });

  // Configurar y crear el gráfico de barras apiladas
  var ctx = document.getElementById('myChart3').getContext('2d');

  new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
      responsive: true,
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true
        }
      }
    }
  });

  // Función para obtener un color aleatorio
  function getRandomColor() {
    var hue = Math.floor(Math.random() * 360);
    var saturation = Math.floor(Math.random() * 30) + 50; // Rango: 50-80
    var lightness = Math.floor(Math.random() * 20) + 50; // Rango: 50-70

    return `hsl(${hue}, ${saturation}%, ${lightness}%)`;

  }
</script>

<script type="text/javascript">
  var influx = <?= json_encode($influx) ?>;
  var days = influx.map(item => item.day);

  var ctx = document.getElementById('myChart4').getContext('2d');

  // Configurar y crear el gráfico de barras
  new Chart(ctx, {
    type: 'bar',
    data: {
      labels: days,
      datasets: [{
        label: 'Importe',
        data: influx.map(item => item.total),
        backgroundColor: 'rgba(54, 162, 235, 0.5)',
        borderColor: 'rgba(54, 162, 235, 1)',
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true,
          ticks: {
            callback: (value) => value // Agregar el símbolo del euro al valor del eje Y
          }
        }
      },
      plugins: {
        tooltip: {
          callbacks: {
            label: context => 'total:' + context.parsed.y // Agregar el símbolo del euro a la etiqueta de información sobre herramientas
          }
        }
      }
    }
  });
</script>



<script type="text/javascript">
var data = {
  labels: [], // Mantener un array vacío para los labels
  datasets: [] // Mantener un array vacío para los datasets
};

// Obtener los datos del JSON y agregarlos al gráfico
var typeLisForm = <?= json_encode($typeWeek) ?>;

// Iterar sobre los datos para obtener los labels y los datasets
typeLisForm.forEach(function(item) {
  // Agregar el día como label
  data.labels.push(item.day);

  // Recorrer los tipos de pago para actualizar los datasets
  item.tipo_pago.forEach(function(pago) {
    var tipoPago = pago.nombre;
    var total = pago.total;

    // Buscar si ya existe un dataset para el tipo de pago
    var existingDataset = data.datasets.find(function(ds) {
      return ds.label === tipoPago;
    });

    // Si ya existe el dataset, agregar el total
    if (existingDataset) {
      var index = data.labels.indexOf(item.day);
      existingDataset.data[index] = total;
    } else {
      // Si no existe, crear un nuevo dataset para el tipo de pago
      var newDataset = {
        label: tipoPago,
        data: Array(data.labels.length).fill(0), // Inicializar con ceros
        backgroundColor: getRandomColor(),
        borderColor: getRandomColor(),
        borderWidth: 1
      };
      var index = data.labels.indexOf(item.day);
      newDataset.data[index] = total;
      data.datasets.push(newDataset);
    }
  });
});

// Configurar y crear el gráfico de barras apiladas
var ctx = document.getElementById('myChart5').getContext('2d');

new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    responsive: true,
    scales: {
      x: {
        stacked: true
      },
      y: {
        stacked: true,
        beginAtZero: true // Asegurarse de que el eje Y comience en 0
      }
    }
  }
});

// Función para obtener un color aleatorio
function getRandomColor() {
  var hue = Math.floor(Math.random() * 360);
  var saturation = Math.floor(Math.random() * 30) + 50; // Rango: 50-80
  var lightness = Math.floor(Math.random() * 20) + 50; // Rango: 50-70

  return `hsl(${hue}, ${saturation}%, ${lightness}%)`;
}
</script>


<script type="text/javascript">
  // Datos del gráfico
  var data = {
    labels: [],
    datasets: []
  };

  // Obtener los datos del JSON y agregarlos al gráfico
  var typeLisForm = [{
      "data": "2023-07-07",
      "day": "viernes",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-08",
      "day": "sábado",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-09",
      "day": "domingo",
      "lisForms": [{
        "description": "default",
        "total": 1
      }]
    },
    {
      "data": "2023-07-11",
      "day": "martes",
      "lisForms": [{
          "description": "Petición de privados",
          "total": 1
        },
        {
          "description": "Petición de aseguradora",
          "total": 1
        },
        {
          "description": "Petición de privados",
          "total": 1
        }
      ]
    }
  ];

  typeLisForm.forEach(function(item) {
    // Agregar las descripciones al array de labels
    data.labels.push(item.day);

    // Crear un dataset para cada tipo de formulario
    item.lisForms.forEach(function(form) {
      var dataset = data.datasets.find(function(ds) {
        return ds.label === form.description;
      });

      // Si el dataset ya existe, agregar el total
      if (dataset) {
        dataset.data.push(form.total);
      } else {
        // Si el dataset no existe, crearlo y agregar el total
        data.datasets.push({
          label: form.description,
          data: [form.total],
          backgroundColor: getRandomColor(),
          borderColor: getRandomColor(),
          borderWidth: 1
        });
      }
    });
  });

  // Configurar y crear el gráfico de barras apiladas
  var ctx = document.getElementById('myChart6').getContext('2d');

  new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
      responsive: true,
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true
        }
      }
    }
  });

  // Función para obtener un color aleatorio
  function getRandomColor() {
    var hue = Math.floor(Math.random() * 360);
    var saturation = Math.floor(Math.random() * 30) + 50; // Rango: 50-80
    var lightness = Math.floor(Math.random() * 20) + 50; // Rango: 50-70

    return `hsl(${hue}, ${saturation}%, ${lightness}%)`;

  }
</script>

<script type="text/javascript">
  // Datos del gráfico
  var data = {
    labels: [],
    datasets: []
  };

  // Obtener los datos del JSON y agregarlos al gráfico
  var typeLisForm = [{
      "data": "2023-07-07",
      "day": "viernes",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-08",
      "day": "sábado",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-09",
      "day": "domingo",
      "lisForms": [{
        "description": "default",
        "total": 1
      }]
    },
    {
      "data": "2023-07-11",
      "day": "martes",
      "lisForms": [{
          "description": "Petición de privados",
          "total": 1
        },
        {
          "description": "Petición de aseguradora",
          "total": 1
        },
        {
          "description": "Petición de privados",
          "total": 1
        }
      ]
    }
  ];

  typeLisForm.forEach(function(item) {
    // Agregar las descripciones al array de labels
    data.labels.push(item.day);

    // Crear un dataset para cada tipo de formulario
    item.lisForms.forEach(function(form) {
      var dataset = data.datasets.find(function(ds) {
        return ds.label === form.description;
      });

      // Si el dataset ya existe, agregar el total
      if (dataset) {
        dataset.data.push(form.total);
      } else {
        // Si el dataset no existe, crearlo y agregar el total
        data.datasets.push({
          label: form.description,
          data: [form.total],
          backgroundColor: getRandomColor(),
          borderColor: getRandomColor(),
          borderWidth: 1
        });
      }
    });
  });

  // Configurar y crear el gráfico de barras apiladas
  var ctx = document.getElementById('myChart7').getContext('2d');

  new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
      responsive: true,
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true
        }
      }
    }
  });

  // Función para obtener un color aleatorio
  function getRandomColor() {
    var hue = Math.floor(Math.random() * 360);
    var saturation = Math.floor(Math.random() * 30) + 50; // Rango: 50-80
    var lightness = Math.floor(Math.random() * 20) + 50; // Rango: 50-70

    return `hsl(${hue}, ${saturation}%, ${lightness}%)`;

  }
</script>

<script type="text/javascript">
  // Datos del gráfico
  var data = {
    labels: [],
    datasets: []
  };

  // Obtener los datos del JSON y agregarlos al gráfico
  var typeLisForm = [{
      "data": "2023-07-07",
      "day": "viernes",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-08",
      "day": "sábado",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-09",
      "day": "domingo",
      "lisForms": [{
        "description": "default",
        "total": 1
      }]
    },
    {
      "data": "2023-07-11",
      "day": "martes",
      "lisForms": [{
          "description": "Petición de privados",
          "total": 1
        },
        {
          "description": "Petición de aseguradora",
          "total": 1
        },
        {
          "description": "Petición de privados",
          "total": 1
        }
      ]
    }
  ];

  typeLisForm.forEach(function(item) {
    // Agregar las descripciones al array de labels
    data.labels.push(item.day);

    // Crear un dataset para cada tipo de formulario
    item.lisForms.forEach(function(form) {
      var dataset = data.datasets.find(function(ds) {
        return ds.label === form.description;
      });

      // Si el dataset ya existe, agregar el total
      if (dataset) {
        dataset.data.push(form.total);
      } else {
        // Si el dataset no existe, crearlo y agregar el total
        data.datasets.push({
          label: form.description,
          data: [form.total],
          backgroundColor: getRandomColor(),
          borderColor: getRandomColor(),
          borderWidth: 1
        });
      }
    });
  });

  // Configurar y crear el gráfico de barras apiladas
  var ctx = document.getElementById('myChart8').getContext('2d');

  new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
      responsive: true,
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true
        }
      }
    }
  });

  // Función para obtener un color aleatorio
  function getRandomColor() {
    var hue = Math.floor(Math.random() * 360);
    var saturation = Math.floor(Math.random() * 30) + 50; // Rango: 50-80
    var lightness = Math.floor(Math.random() * 20) + 50; // Rango: 50-70

    return `hsl(${hue}, ${saturation}%, ${lightness}%)`;

  }
</script>

<script type="text/javascript">
  // Datos del gráfico
  var data = {
    labels: [],
    datasets: []
  };

  // Obtener los datos del JSON y agregarlos al gráfico
  var typeLisForm = [{
      "data": "2023-07-07",
      "day": "viernes",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-08",
      "day": "sábado",
      "lisForms": [{
        "description": "Petición de privados",
        "total": 1
      }]
    },
    {
      "data": "2023-07-09",
      "day": "domingo",
      "lisForms": [{
        "description": "default",
        "total": 1
      }]
    },
    {
      "data": "2023-07-11",
      "day": "martes",
      "lisForms": [{
          "description": "Petición de privados",
          "total": 1
        },
        {
          "description": "Petición de aseguradora",
          "total": 1
        },
        {
          "description": "Petición de privados",
          "total": 1
        }
      ]
    }
  ];

  typeLisForm.forEach(function(item) {
    // Agregar las descripciones al array de labels
    data.labels.push(item.day);

    // Crear un dataset para cada tipo de formulario
    item.lisForms.forEach(function(form) {
      var dataset = data.datasets.find(function(ds) {
        return ds.label === form.description;
      });

      // Si el dataset ya existe, agregar el total
      if (dataset) {
        dataset.data.push(form.total);
      } else {
        // Si el dataset no existe, crearlo y agregar el total
        data.datasets.push({
          label: form.description,
          data: [form.total],
          backgroundColor: getRandomColor(),
          borderColor: getRandomColor(),
          borderWidth: 1
        });
      }
    });
  });

  // Configurar y crear el gráfico de barras apiladas
  var ctx = document.getElementById('myChart9').getContext('2d');

  new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
      responsive: true,
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true
        }
      }
    }
  });

  // Función para obtener un color aleatorio
  function getRandomColor() {
    var hue = Math.floor(Math.random() * 360);
    var saturation = Math.floor(Math.random() * 30) + 50; // Rango: 50-80
    var lightness = Math.floor(Math.random() * 20) + 50; // Rango: 50-70

    return `hsl(${hue}, ${saturation}%, ${lightness}%)`;

  }
</script>
@endsection