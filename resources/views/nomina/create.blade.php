@extends('adminlte::page')
@section('title', 'nominas')

@section('content')
<div class="row">
    <div class="card border shadow mt-2">
        <div class="card-body">
            <h3 class="page_heading">{{ isset($edit) ? 'Editar' : 'Alta' }} de nóminas</h3>

            @if ($errors->any())
            <div class="alert alert-dark alert-dismissible fade show" role="alert">
                <strong>Revise los campos!</strong>
                @foreach($errors->all() as $error)
                <span class="badge badge-danger">{{ $error }}</span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            {!! Form::open(['route' => isset($edit) ? ['nomina.update', $edit->id] : 'nomina.store', 'method' => isset($edit) ? 'PUT' : 'POST']) !!}
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="fecha">Fecha</label>
                        {!! Form::date('fecha', isset($edit) ? $edit->fecha : null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <label for="devengo">Devengo</label>
                        {!! Form::number('devengo', isset($edit) ? $edit->devengo : null, ['class' => 'form-control', 'step' => 'any']) !!}
                    </div>

                    <div class="form-group">
                        <label for="deducciones">Deducciones</label>
                        {!! Form::number('deducciones', isset($edit) ? $edit->deducciones : null, ['class' => 'form-control', 'step' => 'any']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('IRPF', 'IRPF') !!}
                        {!! Form::number('IRPF', isset($edit) ? $edit->IRPF : null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('remuneracion', 'Remuneración') !!}
                        {!! Form::number('remuneracion', isset($edit) ? $edit->remuneracion : null, ['class' => 'form-control', 'step' => 'any']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('empresa_id', 'Empresa', ['class' => 'form-label']) !!}
                        {!! Form::select('empresa_id', $empresa->pluck('nombre', 'id'), isset($edit) ? $edit->empresa_id : null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('comentarios', 'Comentarios') !!}
                        {!! Form::text('comentarios', isset($edit) ? $edit->comentarios : null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <button type="submit" class="btn btn-primary">{{ isset($edit) ? 'Actualizar' : 'Guardar' }}</button>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">

@stop

@section('js')

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@stop