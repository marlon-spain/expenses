@extends('adminlte::page')
@section('title', 'nominas')



@section('content')
<div class="col-lg-12 mt-2">
    <div class="card w-100 h-100">
        <div class="card-body">
            <h7 class="text-right">Registro de nominas: {{ auth()->user()->name }}</h7>

            <br>
            <div class="text-right">
                <a href="{{ route('nomina.create') }}" class="btn btn-primary">Crear Registro</a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-striped table-bordered table-sm" id="items" style="width:100%;">
                    <thead>
                        <tr style="text-align: center;">
                            <th scope="col">id</th>
                            <th scope="col">User</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Devengo</th>
                            <th scope="col">Deducciones</th>
                            <th scope="col">IRPF</th>
                            <th scope="col">Remuneracion</th>
                            <th scope="col">Restante</th>
                            <th scope="col">Empresa</th>
                            <th scope="col">Comentarios</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')

@include('layouts.datatable', [
'ajaxUrl' => route('nomina.index'),
'columns' => json_encode([
['data' => 'id'],
['data' => 'user.name'],
['data' => 'fecha'],
['data' => 'devengo'],
['data' => 'deducciones'],
['data' => 'IRPF'],
['data' => 'remuneracion'],
['data' => 'restante'],
['data' => 'empresa_id'],
['data' => 'comentarios'],
['data' => 'action', 'orderable' => false],
])
])

@include('layouts.delete', ['deleteUrl' => "nomina"])



@if(session('success'))
<script>
    Swal.fire({
        icon: 'success',
        title: 'Éxito',
        text: "{{ session('success') }}",
        timer: 1000,
        showConfirmButton: false // Ocultar el botón de confirmación
    });
</script>
@endif

@if(session('delete'))
<script>
    Swal.fire({
        icon: 'delete',
        title: 'Éxito',
        text: "{{ session('delete') }}",
        timer: 1000,
        showConfirmButton: false // Ocultar el botón de confirmación
    });
</script>
@endif
@endsection