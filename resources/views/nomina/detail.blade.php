
@foreach ($items as $item)
<!--MODAL -->

<div class="modal fade" id="myModal-{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

  <div class="modal-dialog modal-lg modal-dialog-centered">  <!--modal-sm modal-xl  modal-lg-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nomina {{$item->fecha}}</h5>
  

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <table class="table table-striped">
    <thead>
      <tr>
        <th>user</th>
        <th>Fecha</th>
        <th>Devengo</th>
        <th>Deducciones</th>
        <th>IRPF</th>
        <th>Remuneracion</th>
        <th>Restante</th>
        <th>Comentarios</th>
          
      </tr>
    </thead>
    <tbody>     
      <tr>
      <td>{{$item->user->name}}</td>
        <td>{{$item->fecha}}</td>
        <td>{{$item->devengo}}€</td>
        <td>{{$item->deducciones}}€</td>
        <td>{{$item->IRPF}}%</td>
        <td>{{$item->remuneracion}}€</td>
        <td>{{$item->restante}}€</td>
        <td>{{$item->comentarios}}</td>
       
      </tr>
    </tbody>
  </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>  
</div>

<!-- /. modal content-->
@endforeach


