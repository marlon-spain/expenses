@extends('adminlte::page')

@section('title', 'items')

@section('content_header')
<h1>Registro de Sesiones usuarios: {{auth()->user()->name}}</h1>
@stop

@section('content')

<div class="col-lg-12 mt-2">
    <div class="card w-100 h-100">
        <div class="card-body">
            <h7 class="text-right">Registro de Sesiones: {{ auth()->user()->name }}</h7>

            <br>

            <div class="table-responsive">
                <table id="items" class="table table-striped" style="width:100%">
                    <thead>
                        <tr style="text-align: center;">
                            <th scope="col">id</th>
                            <th scope="col">date</th>
                            <th scope="col">active</th>
                            <th scope="col">login</th>
                            <th scope="col">logout</th>
                            <th scope="col">time</th>


                        </tr>

                    </thead>


                </table>
            </div>

        </div>
    </div>
</div>


@stop


@section('js')
@include('layouts.datatable', [
'ajaxUrl' => route('timeUserSession.index'),
'columns' => json_encode([
['data' => 'id'],
['data' => 'date'],
['data' => 'active'],
['data' => 'login'],
['data' => 'logout'],
['data' => 'time', 'orderable' => false],
])
])




@stop